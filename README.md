# README #

General purpose single threaded multiplexed TCP server daemon, for Unix, using LUA as its main scripting language
Written in plain ansi C


Steps for building:

./configure

make

make install


Sample daemon script available inside samples folder