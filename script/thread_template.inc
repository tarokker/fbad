-- sample file for threaded execution

function run(a, b)
    
    -- a test return value
    retval = "my test retval: " .. a .. " - " .. b
        
    -- return value to main thread, an event will be triggered
    return retval
    
end
