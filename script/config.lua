-- Configuration script

-- openserver(ip, port,isadmin)
-- open a new tcp server on which we should listen for connections
--
-- */ip			ip to bind for listen (* = all ip)
-- port			port to open for listen
-- isadministrable	admin users can connect here

-- setthrottle(maxjoin,maxtime,rejint)
-- set throttle config. Throttling can avoid too many connections from same IP.
--
-- thr_max_join		how many times an user can try to connect
-- thr_max_time		interval in seconds to consider to increment throttle counter
-- thr_rej_int		how many seconds to increment user throttling when he's throttled

-- enablevm(mainscriptname, servindex, enable 0/1)
-- enable specified script on specified server
--
-- mainscript		main script of the VM
-- servindex		server index on whicn enable the VM
-- enable 		0/1 - 0:enable - 1:disable

-- setdelim(servindex, delim [, delimlen])
-- set a new packets delimitator for specified server (default \r\n)
-- we can specifify delimlen if we want to use binary delims (with \0 chars)
--
-- NOTE: if delimlen is negative, we'll enable the delimitatorless mode for received packets
-- (for sent packet we will add the specified delimitator (see autodelim), with length = abs(delimlen))
--
-- In delimitatorless mode the daemon won't expect line delimitators, but instead each packet sent to daemon, 
-- HAVE to be prefixed with packet length (unsigned short, big endian)
-- For example (pseudo packet):
-- setdelim(index,"\r\n", -2)
-- autodelim(index,1)
-- RECEIVE:
-- <5><hello>
-- SEND:
-- sendone(index,"blabla",....)
-- "blabla\r\n"

-- autodelim(servindex [,0/1])
-- set or get autodelimitator flag for specified server. if autodelim = 1 the daemon will
-- auto append the delimator after each send

-- flashcheck(servindex [,0/1])
-- set or get flash cross domain check capabilities. if flashcheck = 1 the daemon will check
-- if received text on the socket is a flash crossdomain.xml request and will trigger the policy file request event on the main script

-- autokilloverbuffer(servindex [,0/1])
-- set or get autokill flag for clients which sends packets too large (default 1)

-- mainlooptrigger(steps)                                
-- set onmainloop steps interval (default 0).
--If steps > 0 the onmainloop event will be called every 'steps' (each step is a daemon's step = MAINLOOP_SLEEP ms - defines.h)
        
serv8000 = openserver("*", 8000, 0)
enablevm("template.lua", serv8000, 1)
