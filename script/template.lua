-- Main Script Lua

    -- addban(ip, reason)					-- add ban to specified ip using specified reason. This IP won't be able to connect to server.
    -- admin(index[, 0/1])					-- set or get admin flag for specified client (use .help when connected to know available commands)
    -- chan,pass,users,create_ts = chanuser(index)		-- get user channels list (result format: chan1,pass1,numusers1,timestamp1, chan2, pass2, numusers2...)
    -- chan,pass,users,create_ts = chanlist(minusers/-1)	-- get full channels list, filter by minusers inside them or no filter if -1 (result format same as chanuser)
    -- index1, index2.. = chanusers(chan)			-- get indexes of users inside specified channel
    -- debug(text)						-- log some debug text inside logfile
    -- delban(ip)						-- remove ban for specified ip
    -- include(script)						-- include specified LUA script
    -- joinchannel(clientindex, channame, pass)			-- make specified client join to specified channel, using specified password
    -- killuser(clientindex)					-- kill specified user
    -- listban( [pattern] )					-- search in bans list using specified pattern
    -- crc32(str [,len])					-- calc crc32 value of specified buffer (use len for binary strings which may hold \0 bytes)
    -- md5(str [,len])						-- calc md5 value of specified buffer (use len for binary strings which may hold \0 bytes)
    -- partchannel(clientindex, channame)			-- make specified client part specified channel
    -- rc4(str, pwd, [,len])					-- crypt/decrypt specified buffeer with specified password (use 'len' for binary strings which may hold \0 bytes)
    -- rc4hc(str, pwd, [,len])					-- same as previous, but return value is hex-encoded
    -- rc4hd(str, pwd)						-- decrypt an hex-encoded rc4 string
    -- res = readline([text])					-- blocks execution and prompt user for a line. after the user presses Enter, his input will be returned inside 'res'. 
    --								-- Note: this function is usually used for debug, because it blocks socket processing until user presses Enter (libreadline should be installed)
    --								-- Note2: Daemon MUST be started in single mode (or function will be ignored)
    -- restart							-- restart the server
    -- sendone(toindex, filter, line [,len])			-- send data to specified client (use len for binary strings)
    -- sendallbutone(exindex, filter, line [,len])		-- send data to all users but one (use len for binary strings)
    -- sendall(filter, line [,len])				-- send data to all users (use len for binary srings)
    -- sendchan(name, filter, line [,len])			-- send data to all users in specified channel (by name). Use len for binary strings.
    -- sendchanbutone(name, index, filter, line [,len])		-- send data to all users in specified channel, but specified client (by index). Use len for binary strings)
								--
								-- the filter param for all send* functions, usually should be 0 which means "don't filter sends"
								-- if 'filter' it's > 0 - we'll send data just to clients which had userflag set to 'filter'
								-- if 'filter' it's < 0 - we'll send data to all clients which didn't have userflag set to 'filter'
								-- note: filter will be applied to all clients which should be affected by send* routine in use
								--
    -- settimer(index, timeoutms/-1)				-- set timer callback for specified client (by index). LUA function ontimer will be called every 'timeoutms' milliseconds. Set -1 if you want to remove callback call.
    -- shutdown							-- shutdown the server
    -- thread(filename [p1, p2, p3..])				-- creates a new thread and launches a new LUA vm running 'run' function from 'filename' using specified params. note: threaded routines can't access data from this main script in order to avoid memory access violations isssues
    -- userflag(index [,value])					-- set or get a custom integer value for the specified client (usually used to filter clients in send* functions)
    -- ip, tsjoin, byterecv, bytesent, timeouttimer, 
    -- 		numtimercalls = userinfo(index)			-- retrieve some info for specified user
    
-- function called when the script is loaded
function onload()
    thread("thread_template.inc", 50, "test!")
    thread("thread_template.inc", 150, "test2!")
end

-- function called when the script is unloaded
function onunload()
end

-- function called on every FBAd mainloop step (every X steps, where X is set with mainlooptrigger)
--
function onmainloop(numcalls)
    -- debug("onmainloop event called: " .. numcalls .. " times")
end

-- function called when a client connects to server
function onconnect(index, ip)
    -- ask for a text on local shell and send it to socket (readline must be installed and daemon must be launched in single mode)
    --local res = readline("Write a text: ")
    --sendone(index, 0, "Admin user wrote on shell: " .. res)
    -- send welcome
    sendone(index, 0, "Hi, write your name: ")
    -- set client flag to 1 until client presented itself
    userflag(index, 1)
end

-- callback function called on specified interval (by settimer)
function ontimer(index, numcalls)
end

-- function called when a client has been disconnected
function ondisconnected(index)
end

-- function called when a client receives data on socket
function onread(index, line)    
    -- user is writing his name
    if userflag(index) == 1 then
	-- announce join to other clients with flag 0 (already logged)
	sendall(0, "* " .. line .. " joined chat! Nick: <client:" .. index .. ">")
	-- set his userflag back to 0 and send him welcome
	userflag(index, 0)
	sendone(index, 0, "Welcome " .. line .. " You are <client:" .. index .. "> ! Write /quit to exit")
    else
	-- chat, send line to all logged users or quit if needed
	if line == "/quit" then
	    killuser(index)
	else
	    -- send text to all logged users, but the one who wrote current line
	    sendallbutone(index, 0, "<client: " .. index .. "> " .. line)
	end
    end
end


-- function called when a threaded execution started with 'thread' finishes
-- its return value is passed as paramater
function onthread(data)
    debug("Threaded execution returned: " .. data)
end


-- function called when we're sending data to specified client
function onwrite(index, line)
end
