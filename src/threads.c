/*
 Copyright (c) 2011, Daniele Maiorana <tarokker@tiscali.it>
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
    * Neither the name of the creator nor the names of its contributors may be used to endorse or promote products derived from this software
without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#include "defines.h"
#include "config.h"
#include "struct.h"
#include "extern.h"
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>


// this linked list holds return values of threaded lua functions
// for each item we will signal an event in the main loop
struct return_thread_newlua_func *return_threads_outpars_list = NULL;

// mutex used to lock accesses to previous list
pthread_mutex_t mutex_return_threads_outpars_list = PTHREAD_MUTEX_INITIALIZER;

// mutex used for locking logfile operations
pthread_mutex_t mutex_logfile = PTHREAD_MUTEX_INITIALIZER;

// forward declarations
int _free_threaded_return_values_list();
static void *thread_newlua_func(void *);



// process return values list of threaded lua function
// and signal LUA events to specified LUA VM
//
int flush_threaded_return_values_list( ) {

    // iterate through list and signal LUA events
    struct return_thread_newlua_func *tmp = return_threads_outpars_list;

    while (tmp) {
    
	lua_getglobal(tmp->luaorig, "onthread");
	lua_pushlstring(tmp->luaorig, tmp->data, tmp->datalen);
    
	if ( lua_pcall(tmp->luaorig, 1 /* # passed params */, 0 /*1*/, 0) != 0 )
	    logme(LERROR, "Error while calling onthread callback: %s", lua_tostring(tmp->luaorig, -1));

	tmp = tmp->next;
    }

    // free list
    _free_threaded_return_values_list();
    
    return 0;
    
} // flush_threaded_return_values_list



// free return values list for threaded functions
//
int _free_threaded_return_values_list() {

    // free return values list for threaded functions
    struct return_thread_newlua_func *tmp;

    pthread_mutex_lock( &mutex_return_threads_outpars_list );
    
    while( return_threads_outpars_list ) {

	tmp = return_threads_outpars_list;
	return_threads_outpars_list = return_threads_outpars_list->next;
	
	free(tmp->data);
	free(tmp);
    }
    
    pthread_mutex_unlock( &mutex_return_threads_outpars_list );
    
    return 0;
    
} // free_threaded_return_values_list


// free thread funcs variables (list etc)
//
int free_threads_vars()
{
    
    // free return values list for threaded functions
    _free_threaded_return_values_list();
    
    return 0;
    
} // free_threads_vars


// initialize thread variables (mutex, stack size etc)
//
int init_threads_vars()
{
    return 0;
} // init_threads_vars


// from scripts.c table
// called to create a new thread for a LUA function execution
//
int lua_thread(lua_State *pL)
{
    
    int p, numpars = lua_gettop(pL);
    
    if ( numpars  < 1 ) {
	logme(LERROR, "lua_thread: wrong # of params. Min: 1 - Curr: %d", numpars);
	lua_pushnumber(pL, -1);
	return 1;
    }
    
    // check if 'onthread' callback function is present in current script
    lua_getglobal(pL, "onthread");
    if( !lua_isfunction(pL,-1) )  {
	logme(LERROR, "lua_thread: cannot proceed. calling function is missing: onthread");
	lua_pushnumber(pL, -9);
	return 1;
    }
        
    // we're going to create a new LUA vm and loading the required script
    char tmpfile[1024];
    snprintf(tmpfile, 1024, "%s%s", CURRENT_SCRIPTS_PATH, lua_tostring(pL, 1));
               
    lua_State *lua = create_lua_vm( 1 );      /* import just thread safe functions (md5, rc4..) */
    
    if ( load_lua_script(lua, tmpfile) != 0 ) {
	lua_pushnumber(pL, -2);
	lua_close(lua);
	return 1;
    }

    // check if 'run' function is available inside new loaded script
    lua_getglobal(lua, "run");
    if( !lua_isfunction(lua,-1) )  {
	logme(LERROR, "lua_thread: cannot find 'run' function inside loaded script: %s. Aborting execution.", lua_tostring(pL, 1));
	lua_pushnumber(pL, -3);
	lua_close(lua);
	return 1;
    }
                
    // log new thread
    logme(LVERYDEBUG, "Launching new thread execution for %s (run) - Params: %d", lua_tostring(pL, 1), numpars);
    
    // now we have to push all specified params to 'run' function
    for (p=2; p <= numpars; p++) {
	lua_pushstring(lua, lua_tostring(pL, p));
	/*
	if ( lua_isstring(pL, p) )		lua_pushstring(lua, lua_tostring(pL, p));
	else if ( lua_isnumber(pL, p) )		lua_pushnumber(lua, lua_tonumber(pL, p));
	else if ( lua_isboolean(pL, p) )	lua_pushboolean(lua, lua_toboolean(pL, p));
	else if ( lua_istable(pL, p) )		lua_pushtable(lua, lua_totable(pL, p));
	else if ( lua_isfunction(pL, p) )	lua_pushfunction(lua, lua_tofunction(pL, p));
	*/
    } // push params loop - push params to run function


    // we passed all params, 'lua' VM is ready to run on a new thread
    // set thread stack size
    pthread_t thread;
    pthread_attr_t thread_attr;
    size_t size = sysconf(_SC_THREAD_STACK_MIN) /*PTHREAD_STACK_MIN */ + 0x4000;
    //        
    pthread_attr_init(&thread_attr);
    pthread_attr_setstacksize(&thread_attr, size);
    
    // build params for thread function
    struct pars_thread_newlua_func *pars = malloc(sizeof(struct pars_thread_newlua_func));
    pars->lua = lua;
    pars->luaorig = pL;
    pars->inpars = numpars - 1;
    
    // execute the new thread, if we can or close new VM and return error, if any
    //
    if ( pthread_create(&thread, &thread_attr, thread_newlua_func, (void *)pars) != 0 ) {
	// error, log, close new lua vm and free thread function params struct
	logme(LERROR, "Cannot create a new thread for: %s", lua_tostring(pL, 1) );
	lua_pushnumber(pL, -4);
	lua_close(lua);
	free(pars);
	return 1;
    }
    
    // free thread resources when finished
    pthread_detach( thread );
    
    // main thread continues here...                                            
    lua_pushnumber(pL, 1);
    return 1;
    
} // lua_thread



// function called by 'thread' LUA script command, for execute a new thread
//
static void *thread_newlua_func(void *data)
{
    // cast input params
    struct pars_thread_newlua_func *pars = (struct pars_thread_newlua_func *)data;

    
    // execute LUA 'run' function, previously selected on a new VM by function which created this thread
    //
    if (lua_pcall(pars->lua, pars->inpars /* # passed params */, 1 /*1*/, 0) != 0) {
	// error while executing
	logme(LERROR, "LUA threaded function error: %s", lua_tostring(pars->lua, -1));
    } else {

	// execution ok, read return strings and create items for events post-signal
	// this will be processed in the main thread
	pthread_mutex_lock( &mutex_return_threads_outpars_list );
	
	const char *ret = lua_tostring(pars->lua, -1);
	
	// create new return value item and place it at the end of the list
	struct return_thread_newlua_func *tmpitem = return_threads_outpars_list, 
		    *retitem = malloc(sizeof( struct return_thread_newlua_func ));
	retitem->datalen = lua_rawlen(pars->lua, -1);
	retitem->data = (char *)malloc( sizeof(char) * retitem->datalen );
	memcpy(retitem->data, ret, retitem->datalen);
	retitem->luaorig = pars->luaorig;	// save pointer to main lua VM
	retitem->next = NULL;
	
	if ( tmpitem ) {
	    // middle item, search the last one and place new item there
	    while( tmpitem->next )
		tmpitem = tmpitem->next;
		
	    tmpitem->next = retitem;
	    
	} else {
	    // first item, will be the head
	    return_threads_outpars_list = retitem;
	}
	
	lua_pop(pars->lua, 1);
	pthread_mutex_unlock( &mutex_return_threads_outpars_list );
	
    } // execution ok
    
    // when we have finished with threaded LUA execution
    // close lua vm
    lua_close(pars->lua);
    
    // free input params struct
    free(pars);
    
    return 0;
    
} // thread_newlua_func
