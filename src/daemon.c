/*
 Copyright (c) 2011, Daniele Maiorana <tarokker@tiscali.it>
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.
    * Neither the name of the creator nor the names of its contributors may be used to endorse or promote products derived from this software 
without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/

#include "defines.h"
#include "extern.h"
#include "config.h"
#include "struct.h"


#include <errno.h>
#include <event.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <unistd.h>
// disable on macos
#ifndef __APPLE__
#include <sys/prctl.h>
#endif

// flag for mainloop return values
static char exit_mainloop = 0;

// check if we're child process (default: -1)
// if it's 0, we're child
int pid = -1;

unsigned int throttle_allowed_num = 0;                  // how many clients can connect
unsigned int throttle_allowed_interval = 0;             // time interval in which the clients can connect
unsigned int throttle_reject_interval = 0;              // reject interval to be added each time a client has been throttled
	    
// buffer used in socket read routine
// (it will be copied in client's buffer)
static char sockbuffer[MAX_RECV_BUFFER+1];

// useful temp structs used in incoming-connection functions in order to avoid
// lot of stack allocation/deallocation
unsigned int IN_ADDR_LEN = sizeof(struct sockaddr_in);
int IN_ADDR_SOCK;
struct sockaddr_in IN_ADDR;

// sockets servers
struct sockserv **SERVERS = NULL;
unsigned short NUMSERVERS = 0;

struct sockclient **CLIENTS = NULL;
unsigned short CLIENTSMAX = 0;

// full created channels list
struct chanlist *chanlist = NULL;

// daemon launch timestamp
time_t timestamp_start;

// clients throttled list
struct throttle_list *throttle_list = NULL;

// chanlist hashlist - used to find quickly a channel, from its name
struct chanlist *hashlist[CHANS_HASHLIST];

// how much mainloop steps we should do before calling LUA onmainloop event (default = 0, never trigger lua onmainloop)
unsigned int lua_onmainloop_trigger_step = 0, lua_onmainloop_trigger_max = 0;
unsigned long lua_onmainloop_trigger_count = 0;	// how many times the event has been triggered

// forward declarations
int check_admin_commands(int, const char *, int);
int check_throttling(char *ip);
int clear_throttling();
struct channel *findchannel(const char *);
int _general_send_one_nosanity(int, const char *, unsigned int);
int handle_incoming_connections();
int hashindex(const char *name);
int killuser(int);
static void onsockevent(void *, int, int, struct ev_ct *);
static void onsocktimer(void *, struct ev_tm *, struct ev_ct *);
int partallchannels(int);
void setup_corefile();
void signal_handler(int);



// check if we have to throttle the specified ip
// return 0 if we can accept the client else the number of seconds the
// user should be throttled
//
int check_throttling(char *ip)
{

    // fast check - if any of 3 throttle config values is 0, allow connection to all
    if ( throttle_allowed_num == 0 || throttle_allowed_interval == 0 || throttle_reject_interval == 0 )
	return 0;
	
    // iterate throttle list, removing inactive clients
    struct throttle_list *prevthrottle = NULL, *currthrottle = throttle_list, *foundthrottle = NULL, *delthrottle;
    
    while (currthrottle) {
    
	// save throttle if it has our ip
	if ( stricmp(currthrottle->ip, ip) == 0 )
	    foundthrottle = currthrottle;
	
	// if throttle item expired, remove it from throttle list
	if ( time(NULL) > currthrottle->tslast + currthrottle->throttle + throttle_allowed_interval ) {
	
	    // if it was our savd ip, set pointer to NULL because we're going to remove it
	    if ( stricmp(currthrottle->ip, ip) == 0 )
		foundthrottle = NULL;
	    
	    delthrottle = currthrottle;
	    logme(LDEBUG, "Removing expired throttle check - IP: %s - Last TS: %d - Joins: %d", delthrottle->ip, delthrottle->tslast, delthrottle->numjoin);
	    
	    // relink list
	    if ( prevthrottle ) {
		// middle: A->[B]->C
		prevthrottle->next = delthrottle->next;
		currthrottle = delthrottle->next; // continue check from C
		// prev is still A
	    } else {
		// head:  [A]->B->C
		currthrottle = currthrottle->next;	// continue check from B
		throttle_list = currthrottle; // updated head
		// prev is still NULL
	    }
	    
	    // free item
	    free(delthrottle->ip);
	    free(delthrottle);
	    
	} // throttle item to be removed
	
	prevthrottle = currthrottle;
	if ( currthrottle ) 	// check if current node is still valide (we could had delete it previously)
	    currthrottle = currthrottle->next;
    } // throttle list cleaning loop 
    
    
    // if client wasn't in throttle list, create item now
    if ( !foundthrottle ) {
	
	// client not in throttle list yet, add it now
	foundthrottle = (struct throttle_list *)malloc(sizeof(struct throttle_list));
	foundthrottle->ip = (char *)malloc(strlen(ip)+1);
	strncpy2(foundthrottle->ip, ip, strlen(ip)+1);
	foundthrottle->tslast = time(NULL);
	foundthrottle->throttle = 0;
	foundthrottle->numjoin = 1;
	foundthrottle->next = throttle_list;
	throttle_list = foundthrottle;

	logme(LVERYDEBUG, "Client: %s - Added in throttle list - TS: %ld", ip, foundthrottle->tslast);
	
	return 0;	// throttle just created - allow connection
	
    } // client not in list yet, create throttle item
    else {
	// if client was already in throttle list, increment numjoin and check
	// if we have to throttle it
	//
	time_t tslast = foundthrottle->tslast;
	foundthrottle->tslast = time(NULL);	// update lastjoin to now
	
	// if it's joining inside our defined interval, increment numjoins
	if ( tslast + throttle_allowed_interval >= time(NULL) ) {
	    ++foundthrottle->numjoin;
	    // if we got too much joins in the interval, increment throttling and return the number
	    if ( foundthrottle->numjoin > throttle_allowed_num ) {
		foundthrottle->throttle += throttle_reject_interval;
		logme(LDEBUG, "Client: %s - Joins: %d - Reached connect limit - Throttled for: %d secs", ip, foundthrottle->numjoin, foundthrottle->throttle);
		return foundthrottle->throttle;
	    }
	} // it's joining in the specified interval
    } // client already in throttle list
    
    // allow connections in other cases (?)
    return 0;
    
} // check_throttling


// clear throttle list
//
int clear_throttling()
{
    int ret = 0;
    struct throttle_list *tempthrottle;
    while (throttle_list) {
	tempthrottle = throttle_list;	throttle_list = throttle_list->next;
	free(tempthrottle->ip);	free(tempthrottle); tempthrottle = NULL;
	++ret;
    }
    return ret;
} // clear_throttling

// check specified line in order to check if there are some admin commands
// check for permissions is done by calling function
//
int check_admin_commands(int clientindex, const char *line, int linelen) {

    int s, handled = 0;
    
    if ( linelen == 0 ) 
	return handled;
    
    char *cmd = (char *)malloc(sizeof(char) * (linelen+1));
    const int tmpbuflen = MAX_RECV_BUFFER + 200;
    char tmpbuf[tmpbuflen + 1];
    char ip_address[INET_ADDRSTRLEN];
    
    // create command line
    for (s=0; s < linelen; s++)
	cmd[s] = line[s];
    cmd[linelen]='\0';
    
    // extract command and parameters
    char *firstw = strtok(cmd, " ");
    char *par1 = strtok(NULL, " ");
    
    if ( stricmp(firstw, ".help") == 0 ) {
	general_send(0,NULL, clientindex, 0, "[000] --------------- Commands List ---------------", -1);
	general_send(0,NULL, clientindex, 0, "[000] .addban ip reason - add a ban to bans list", -1);
	general_send(0,NULL, clientindex, 0, "[000] .banslist *wild*  - show full bans list", -1);
	general_send(0,NULL, clientindex, 0, "[000] .channels *wild*  - show full channels list", -1);
	general_send(0,NULL, clientindex, 0, "[000] .chanusers name   - show channel users list", -1);
	general_send(0,NULL, clientindex, 0, "[000] .clearbans        - clear bans list", -1);
	general_send(0,NULL, clientindex, 0, "[000] .clients *wild*   - show clients list", -1);
	general_send(0,NULL, clientindex, 0, "[000] .delban ip        - remove ban from specified ip", -1);
	general_send(0,NULL, clientindex, 0, "[000] .kill id          - kill client specified by id", -1);
	general_send(0,NULL, clientindex, 0, "[000] .msg id text      - send text to client id", -1);
	general_send(0,NULL, clientindex, 0, "[000] .restart          - restart server", -1);
	general_send(0,NULL, clientindex, 0, "[000] .shutdown         - shutdown server", -1);
	general_send(0,NULL, clientindex, 0, "[000] .throttle         - clear throttle list", -1);
	general_send(0,NULL, clientindex, 0, "[000] .uptime           - show daemon uptime", -1);
	general_send(0,NULL, clientindex, 0, "[000] ---------------------------------------------", -1);
	handled++;
    }
    else if ( stricmp(firstw, ".banslist") == 0 ) {
	unsigned long bancount = 0;
	start_bans_navigate();
	struct bans_list *ban = get_next_ban(par1);
	    
	snprintf(tmpbuf, tmpbuflen, "[063] --------- Bans List For: %s ---------", (par1?par1:"ALL"));
	general_send(0,NULL, clientindex, 0, tmpbuf, -1);
	    
	while ( ban ) {
	    ++bancount;
	    
	    snprintf(tmpbuf, tmpbuflen, "[064] %06ld> %s (%s)", bancount, ban->ip, ban->reason);
	    general_send(0,NULL, clientindex, 0, tmpbuf, -1);
	
	    ban = get_next_ban(par1);
	}
	snprintf(tmpbuf, tmpbuflen, "[065] %06d> Total Bans Found: %ld", 999999, bancount);
	general_send(0,NULL, clientindex, 0, tmpbuf, -1);
	general_send(0,NULL, clientindex, 0, "[066] ---------------------------------------------", -1);
	handled++;
    }
    else if ( stricmp(firstw, ".clearbans") == 0 ) {
	clear_bans();
	snprintf(tmpbuf, tmpbuflen, "[061] Bans list cleared");
	save_bans_to_file();
	general_send(0,NULL, clientindex, 0, tmpbuf, -1);
	handled++;
    }
    else if ( stricmp(firstw, ".delban") == 0 && par1 ) {
	int resdelban = delban( par1 );
	int savebanres = -9;
	if ( resdelban > 0 ) savebanres = save_bans_to_file();
	snprintf(tmpbuf, tmpbuflen, "[062] Del ban, result: %d - save: %d", resdelban, savebanres);
	general_send(0,NULL, clientindex, 0, tmpbuf, -1);
	handled++;	
    }
    else if ( stricmp(firstw, ".addban") == 0 && par1 ) {
	char *tmpdest = par1;
	strncpy2(tmpbuf, "", tmpbuflen);
	par1 = strtok(NULL, " ");
	while (par1) {
	    if (strlen(tmpbuf) > 0) strncat(tmpbuf, " ", tmpbuflen - strlen(tmpbuf) - 2);
	    strncat(tmpbuf, par1, tmpbuflen - strlen(tmpbuf) - 2);
	    par1 = strtok(NULL, " ");
	}
	int addbanres = addban(tmpdest, tmpbuf);
	int savebanres = -9;
	if ( addbanres > 0 ) savebanres = save_bans_to_file();
	snprintf(tmpbuf, tmpbuflen, "[060] Add ban, result: %d - save: %d", addbanres, savebanres);
	general_send(0,NULL, clientindex, 0, tmpbuf, -1);
	handled++;
    }
    else if ( stricmp(firstw, ".msg") == 0 && par1 ) {
	char *tmpdest = par1;
	strncpy2(tmpbuf, "", tmpbuflen);
	par1 = strtok(NULL, " ");
	while (par1) {
	    if (strlen(tmpbuf) > 0) strncat(tmpbuf, " ", tmpbuflen - strlen(tmpbuf) - 2);
	    strncat(tmpbuf, par1, tmpbuflen - strlen(tmpbuf) - 2);
	    par1 = strtok(NULL, " ");
	}
	if ( general_send(0,NULL, atoi(tmpdest), 0, tmpbuf, -1) >= 0 ) {
	    general_send(0,NULL, clientindex, 0, tmpbuf, -1);
	} else {
	    snprintf(tmpbuf, tmpbuflen, "[021] Unable to send text to: %d", atoi(tmpdest));
	    general_send(0,NULL, clientindex, 0, tmpbuf, -1);
	}
	handled++;
    }
    else if ( stricmp(firstw, ".kill") == 0 && par1 ) {
	if ( atoi(par1) == clientindex )
	    snprintf(tmpbuf, tmpbuflen, "[006] Cannot kill yourself");
	else if ( killuser(atoi(par1)) >= 0 ) {
	    snprintf(tmpbuf, tmpbuflen, "[007] User %s killed", par1);
	    logme(LNORMAL, "Admin client: %d - asked for user kill: %s", clientindex, par1);
	}
	else
	    snprintf(tmpbuf, tmpbuflen, "[008] Cannot kill user %s. Not found.", par1);
	general_send(0,NULL, clientindex, 0, tmpbuf, -1);
	handled++;
    }
    else if ( stricmp(firstw, ".chanusers") == 0 && par1 ) {
	struct channel *chan = findchannel(par1);
	if ( !chan ) {
	    snprintf(tmpbuf, tmpbuflen, "[009] Error: channel %s not found", par1);
	    general_send(0,NULL, clientindex, 0, tmpbuf, -1);
	} else {
	    int chanusers = 0;
	    struct clientlist *cll = chan->clientlist;
	    snprintf(tmpbuf, tmpbuflen, "[010] ------------- Channel: %s users ----------", par1);
	    general_send(0, NULL, clientindex, 0, tmpbuf, -1);
	    while ( cll ) {
		++chanusers;		
	    	inet_ntop(AF_INET, &(cll->client->addr.sin_addr), ip_address, INET_ADDRSTRLEN);
		snprintf(tmpbuf, tmpbuflen, "[011] %06d> (ID:%05d) IP: %s - PORT: %d - JOIN: %ld - RECV/SEND: %ld/%ld", chanusers, cll->client->id, ip_address, cll->client->addr.sin_port, cll->client->tsjoined, cll->client->numrecv, cll->client->numsend);
		general_send(0, NULL, clientindex, 0, tmpbuf, -1);
		cll = cll->next;
	    }
	    snprintf(tmpbuf, tmpbuflen, "[012] %06d> Total Found: %d", 999999, chanusers);
	    general_send(0, NULL, clientindex, 0, tmpbuf, -1);
	    general_send(0, NULL, clientindex, 0, "[000] ----------------------------------------------", -1);
	}
	handled++;
    }
    else if ( stricmp(firstw, ".channels") == 0 ) {
	int chancounter = 0;
	struct chanlist *cl = chanlist;
	general_send(0, NULL, clientindex, 0, "[013] --------------- Channels List  ---------------", -1);
	while (cl) {
	    if ( !par1 || match(par1, cl->chan->name) == 0 ) {
		++chancounter;
		snprintf(tmpbuf, tmpbuflen, "[014] %06d> %s [%s] - CREATE: %ld - USERS: %d", chancounter, cl->chan->name, cl->chan->pass, cl->chan->created_time, cl->chan->numusers);
		general_send(0, NULL, clientindex, 0, tmpbuf, -1);
	    }
	    cl = cl->next;
	}
	snprintf(tmpbuf, tmpbuflen, "[015] %06d> Total Found: %d", 999999, chancounter);
	general_send(0, NULL, clientindex, 0, tmpbuf, -1);
	general_send(0, NULL, clientindex, 0, "[000] ----------------------------------------------", -1);
	handled++;
    }
    else if ( stricmp(firstw, ".clients") == 0 ) {
	unsigned long counter = 0;
	general_send(0, NULL, clientindex, 0, "[016] --------------- Clients List  ---------------", -1);
	for (s=0; s < CLIENTSMAX; s++) {
	    if ( !CLIENTS[s] )
		continue;
	    inet_ntop(AF_INET, &(CLIENTS[s]->addr.sin_addr), ip_address, INET_ADDRSTRLEN);
	    if ( !par1 || match(par1, ip_address) == 0 ) {
		snprintf(tmpbuf, tmpbuflen, "[017] %05d> (ID:%05d) IP: %s - PORT: %d - JOIN: %ld - RECV/SEND: %ld/%ld", s+1, CLIENTS[s]->id, ip_address, CLIENTS[s]->addr.sin_port, CLIENTS[s]->tsjoined, CLIENTS[s]->numrecv, CLIENTS[s]->numsend);
		general_send(0, NULL, clientindex, 0, tmpbuf, -1);
		counter++;
	    }
	}
	snprintf(tmpbuf, tmpbuflen, "[018] %05d> Total: %ld", 99999, counter);
	general_send(0, NULL, clientindex, 0, tmpbuf, -1);
	general_send(0, NULL, clientindex, 0, "[000] ---------------------------------------------", -1);
	handled++;
    }
    else if ( stricmp(firstw, ".throttle") == 0 ) {
	logme(LNORMAL, "Admin client: %d - asked for throttle list clear", clientindex);
	snprintf(tmpbuf, tmpbuflen, "[030] throttle list cleared. Removed: %d items", clear_throttling());
	general_send(0, NULL, clientindex, 0, tmpbuf, -1);
	handled++;
    }
    else if ( stricmp(firstw, ".shutdown") == 0 ) {
	logme(LNORMAL, "Admin client: %d - asked for server shutdown", clientindex);
	general_send(0, NULL, clientindex, 0, "[001] server is shutting down...", -1);
	rebootshutdown(1 /* shutdown */);
	handled++;
    }
    else if ( stricmp(firstw, ".restart") == 0 ) {
	logme(LNORMAL, "Admin client: %d - asked for server restart", clientindex);
	general_send(0, NULL, clientindex, 0, "[002] server is restarting...", -1);
	rebootshutdown(2 /* restart */);
	handled++;
    }
    else if ( stricmp(firstw, ".uptime") == 0 ) {
	struct tm *loctime = localtime(&timestamp_start);
	snprintf(tmpbuf, tmpbuflen, "[040] Server start: %02d/%02d/%04d - %02d:%02d:%02d", loctime->tm_mday, loctime->tm_mon, 1900+loctime->tm_year,
	            loctime->tm_hour, loctime->tm_min, loctime->tm_sec);
	general_send(0, NULL, clientindex, 0, tmpbuf, -1);		    
	handled++;
    }
                
    free(cmd);	cmd = NULL;

    return handled;
        
} // void check_admin_commands


// client specified by index is valid (socket connected)
//
int client_valid(int index) {
    if (CLIENTSMAX == 0 || index >= CLIENTSMAX || !CLIENTS[index])
	return 0;
    else
	return 1;
} // client_valid

// find a channel by name, searching in hashlist
// return channel pointer or NULL if not found
//
struct channel *findchannel(const char *name) {
    
    int ihash = hashindex ( name );
    
    struct chanlist *cl = hashlist[ ihash ];
    
    while (cl) {
	// channel found
	if (strcmp(cl->chan->name, name) == 0)
	    return cl->chan;
	cl = cl->next;
    }
    
    // nothing found
    return NULL;
    
} // findchannel


// send specified buffer to specified client
// no sanity checks are made on index
//
int _general_send_one_nosanity(int toindex, const char *line, unsigned int linelen) {
    
    // send data to socket    
    int retwrite = write(CLIENTS[toindex]->sock, line, linelen);	// send data
    CLIENTS[toindex]->numsend += linelen;	// increment sent bytes counter
    
    // Autosend delimitator if required
    if ( CLIENTS[toindex]->server->autodelim == 1 ) {
	retwrite = write(CLIENTS[toindex]->sock, CLIENTS[toindex]->server->sockdelim, CLIENTS[toindex]->server->sockdelimsize);	// add delimitator if required
	CLIENTS[toindex]->numsend += CLIENTS[toindex]->server->sockdelimsize; // increment sent bytes counter
    }
    
    // signal write to LUA
    signal_line_read_write(CLIENTS[toindex], line, linelen, 1 /* iswrite */);

    if ( retwrite != 0 ) {} /* make compiler happy */

    return 1;
} // _general_send_one


// generic function for socket data send
// filter - if 0, send to all users which match specified send type (channels, all, butone etc)
//          if > 0 send to all users which had their userflag set to 'filter'
//	    if < 0 send to all users which didn't have their userflag set to 'filter'
// chan - if not NULL, send to a channel
// exclude - we need to exclude the specified client (used on mass-sends)
// index - client index on which send data (-1 = send all, or ignore if exclude = 1)
// line - buffer to sent
// linelen = buffer to sent length (if -1, consider string as \0 terminated, so use strlen to calculate length)
//
int general_send(long filter, const char *chan, int index, char exclude, const char *line, int linelen) {

    int c,numsend = 0;
    //long absfilter = abs(filter);
    
    // send to all channel users or to an user or to all user
    if (chan) {
    
	// send to all channel users
	struct channel *ch = findchannel(chan);
	
	if ( !ch ) {
	    logme(LVERYDEBUG, "Trying to send data to unexistant chan: %s . Ignoring", chan);
	    return -1;	// channel not found
	}
	
	// iterate all users in the channel and send data to all of them (or exclude one of them if specified)
	struct clientlist *cll = ch->clientlist;
	
	while (cll) {
	    if (
		(exclude == 0 || (exclude != 0 && cll->client->id != index)) 
		&&
		( filter == 0 || filter == cll->client->flag1 )
		/* ( (filter == 0) || (filter > 0 && cll->client->flag1 == absfilter) || (filter < 0 && cll->client->flag1 != absfilter) ) */
	    )
		numsend += _general_send_one_nosanity(cll->client->id, line, ( linelen < 0 ? strlen(line): linelen ) );
	    cll = cll->next;
	}
	
    } else if ( index >= 0 && exclude == 0 ) {
    
	// send data to specified user
	if ( client_valid(index) == 0 ) {
	    logme(LVERYDEBUG, "Trying to send data to unexistant user: %d . Ignoring", index);
	    return -2;	// client not valid
	}
	
	/* if ( (filter == 0) || (filter > 0 && CLIENTS[index]->flag1 == absfilter) || (filter < 0 && CLIENTS[index]->flag1 != absfilter) ) */
	if ( filter == 0 || filter == CLIENTS[index]->flag1 )
	    numsend += _general_send_one_nosanity(index, line, ( linelen < 0 ? strlen(line): linelen ) );
	
    } else {

	// send data to ALL clients
	for (c = 0; c < CLIENTSMAX; c++) {
	
	    // client valid
	    if ( !CLIENTS[c] )
		continue;
	    // all users, exclude one if specified
	    if (
		(exclude == 0 || (exclude != 0 && CLIENTS[c]->id != index))
		&&
		/* ( (filter == 0) || (filter > 0 && CLIENTS[c]->flag1 == absfilter) || (filter < 0 && CLIENTS[c]->flag1 != absfilter) ) */
		( filter == 0 || filter == CLIENTS[c]->flag1 )
	    )		
		numsend += _general_send_one_nosanity(CLIENTS[c]->id, line, ( linelen < 0 ? strlen(line): linelen ) );
		
	}
	
    }
    
    return numsend;
    
} // general_send



// check if specified user is admin
//
int get_admin_client(int index) {

    // invalid client
    if ( client_valid(index) == 0 )
	return -1;
	
    return (int)CLIENTS[index]->isadmin;
    
} // get_admin_client


// get full channels list, filtering by minimum users number if specified
//
struct chanlist *get_fullchan_list(int minusers) {

    // create channels list, filtered using specified criteria
    struct chanlist *retcl = NULL, *newcl, *cl = chanlist;
    
    while (cl) {
	// add channel if it had required minimum users
	if ((minusers == -1) || (cl->chan->numusers >= minusers)) {
	    newcl = (struct chanlist *)malloc(sizeof(struct chanlist));
	    newcl->chan = cl->chan;
	    newcl->next = retcl;
	    retcl = newcl;
	}
	cl = cl->next;
    }
    
    return retcl;

} // get_fullchan_list


// get channels list of specified user
//
struct chanlist *get_chan_list(int index) {
    
    // invalid client
    if ( client_valid(index) == 0 )
	return NULL;

    // create channels list of the user
    struct sockclient *client = CLIENTS[index];
    struct chanlist *retcl = NULL, *newcl, *cl = client->chanlist;
    
    while (cl) {
	newcl = (struct chanlist *)malloc(sizeof(struct chanlist));
	newcl->chan = cl->chan;
	newcl->next = retcl;
	retcl = newcl;
	cl = cl->next;
    }
    
    return retcl;
    
} // get_chan_list



// handle incoming connections from current opened servers
//
int handle_incoming_connections() {

    if ( NUMSERVERS == 0 )
	return 0;
    
    int s, found, i, oldsize;
    char ip_address[INET_ADDRSTRLEN];
    
    // iterate all servers
    //    
    for (s = 0; s < NUMSERVERS; s++) {
    
	// try to accept connections
	IN_ADDR_SOCK = accept(SERVERS[s]->sock, (struct sockaddr *)&IN_ADDR, &IN_ADDR_LEN);

	if ( IN_ADDR_SOCK == -1 && ( errno == EAGAIN || errno == EWOULDBLOCK) )
	    continue;	// no connection
	else if ( IN_ADDR_SOCK >= 0 ) {

	    // socket got valid handle
	    
	    // set socket keep alive
	    int yes = 1;
	    
	    if(setsockopt(IN_ADDR_SOCK,SOL_SOCKET,SO_KEEPALIVE,&yes,sizeof(int))<0) {
		logme(LERROR, "setsockopt - SO_KEEPALIVE - failed - %d", IN_ADDR_SOCK);
		shutdown( IN_ADDR_SOCK, SHUT_RDWR);
		close( IN_ADDR_SOCK );
		continue; // try with next server
	    }
				   
	    // get info of connected client
	    getpeername(IN_ADDR_SOCK,(struct sockaddr*)&IN_ADDR,&IN_ADDR_LEN);

	    // if user is banned, don't let him enter
	    inet_ntop(AF_INET, &(IN_ADDR.sin_addr), ip_address, INET_ADDRSTRLEN);
	    
	    if ( is_banned(ip_address) == 1 ) {
		// log is done inside is_banned
		shutdown( IN_ADDR_SOCK, SHUT_RDWR);
		close( IN_ADDR_SOCK );
		continue; // try with next server
	    }
	    
	    // accept connection or throttle user if needed
	    found = check_throttling( ip_address );
	    
	    if ( found  > 0 ) {
		logme(LDEBUG, "Incoming connection rejected: %d - Client: %s:%d - Throttled for: %d seconds", IN_ADDR_SOCK, ip_address, IN_ADDR.sin_port, found);
		shutdown( IN_ADDR_SOCK, SHUT_RDWR);
		close( IN_ADDR_SOCK );
		continue; // try with next server
	    }
	    else
		logme(LDEBUG, "Incoming connection accepted: %d - Client: %s:%d", IN_ADDR_SOCK, ip_address, IN_ADDR.sin_port);
	    
	    // set non-blocking socket
	    if ( fcntl(IN_ADDR_SOCK, F_SETFL, fcntl(IN_ADDR_SOCK, F_GETFL, 0) | O_NONBLOCK) < 0) {
		logme(LERROR, "Cannot set client socket non blocking: %d", IN_ADDR_SOCK);
		shutdown( IN_ADDR_SOCK, SHUT_RDWR );
		close(IN_ADDR_SOCK);
		continue;	// try with next server
	    }

	    
	    // create sockclient struct
	    struct sockclient *client = (struct sockclient *)malloc(sizeof(struct sockclient));
	    client->server = SERVERS[s];
	    client->sock = IN_ADDR_SOCK;

	    client->bufferlen = 0;
	    strncpy2(client->buffer, "", MAX_RECV_BUFFER+1);
	    strncpy2(client->ip, ip_address, MAX_IP_LEN);	// client ip
	    
	    client->chanlist = NULL;	// not in channels yet
	    
	    memcpy(&(client->addr), &IN_ADDR, sizeof(struct sockaddr_in) );

	    // we have to save pointer in the list of connected clients
	    // search a NULL client
	    found = -1;
    	    if (CLIENTSMAX > 0) {
        	for (i = 0; i < CLIENTSMAX; i++) {
            	    if ( !CLIENTS[i] ) {
                	found = i;
                	break;
            	    }
        	}
    	    }
	    // nothing found, we need to allocate more space
    	    if ( found == -1 ) {
        	// reallocate
        	oldsize = CLIENTSMAX;
        	CLIENTSMAX += REALLOC_CLIENTS_PAGE;
        	struct sockclient **NEWCLIENTS = (struct sockclient **)realloc(CLIENTS, sizeof(struct sockclient *) * CLIENTSMAX);
        	if ( !NEWCLIENTS ) {
            	    logme(LERROR, "Unable to reallocate clients sockets array");
		    free(client);
            	    close(IN_ADDR_SOCK);
            	    continue;	// goto next server
        	} else {
            	    logme(LDEBUG, "Reallocating CLIENTS array from %ld to %ld", oldsize, CLIENTSMAX);
        	}
        	CLIENTS = NEWCLIENTS;
		// set NULL on new allocated clients
        	for (i = oldsize; i < CLIENTSMAX; i++)
            	    CLIENTS[i]=NULL;
		// save on first free slot
        	found = oldsize;
    	    }
	    // save pointer on found slot
	    client->id = found;
	    client->isadmin = 0;	// by default client is not admin
	    client->flag1 = 0;		// default user-defined flag: 0
	    client->tsjoined = time(NULL);	// when we entered
	    client->numrecv = 0;		// no bytes received
	    client->numsend = 0;		// no bytes sent
	    client->server->numclients++;	// one more user connected on this server
	    	    
	    client->timer = NULL;		// no timers active by default
	    client->timeout = -1;
	    client->calltimes = 0;
	    
    	    CLIENTS[found] = client;
	    
	    // register callback for read/write events on socket libevent
	    if ( ev_io_add(NULL, client->sock, EV_IN, (ev_io_cbck_f *)onsockevent,  (void *)client) == -1 ) {
		logme(LERROR, "Unable to add event for socket library: %d - Err(%d): %s", client->sock, errno, strerror(errno));
		close(IN_ADDR_SOCK);
		free(client);
		continue;	// try with next server
	    }
	    	    
	    // log
	    logme(LDEBUG, "Connection from IP: %s:%d - Connection to: %s:%d (ID: %ld, FD: %d)", ip_address, IN_ADDR.sin_port, client->server->listen_address, client->server->listen_port, client->id, client->sock);
	    
	    // signal to LUA a new client connection
	    signal_client_connection(1 /* connected */, client);
	    
	} // valid handle - connection accepted
	else {
	    // unable to complete socket accept
	    logme(LERROR, "Unable to accept connection for socket: %d - Server: %d - Err: (%d) - %s", SERVERS[s]->sock, s, errno, strerror(errno));
	    continue;	// try with next server
	}
	
    }	// servers loop
    
    return 0;
    
} // int handle_incoming_connections


// get channel hashlist index for the specified channel name
//
int hashindex(const char *name) {
    if (strlen(name) < 2)
	return CHANS_HASHLIST - 1;
    else
	return name[0] + name[1];
}

// join specified client to the specified channel, using optional password
//
int joinchannel(int index, const char *name, const char *pass) {

    // client not found
    if ( client_valid(index) == 0 )
	return -1;

    // check if user was already in the channel
    struct sockclient *client = CLIENTS[index];
    struct chanlist *cl = client->chanlist;
    
    while (cl) {
	// channel found
	if (strcmp(cl->chan->name, name) == 0)
	    return -2;  // user already in channel
	cl = cl->next;
    }
					    
    
    // search channel
    struct channel *chan = findchannel(name);
    
    // if channel has password and it doesn't match the given one, we can't join
    if ( chan ) {
	if ( strlen(chan->pass) > 0 && (!pass || strcmp(chan->pass, pass) != 0) )
	    return -3;	// invalid password
    }
    
    
    char just_created = 0;
    
    // if we didn't find channel, create it now
    if ( !chan ) {
	// create channel
	chan = (struct channel *)malloc(sizeof(struct channel));
	strncpy2(chan->name, name, MAX_CHANNEL_NAME+1);
	if ( pass )
	    strncpy2(chan->pass, pass, MAX_CHAN_PASSWORD + 1);
	else
	    strncpy2(chan->pass, "", MAX_CHAN_PASSWORD + 1);
	chan->numusers = 0;
	chan->clientlist = NULL;
	chan->created_time = time(NULL);
	logme(LDEBUG, "Created channel: %s - Pass: %s", name, (pass?pass:""));
	// add new channel to global channels list
	struct chanlist *foundcl = (struct chanlist *)malloc(sizeof(struct chanlist));
	foundcl->next = chanlist;
	foundcl->chan = chan;
	chanlist = foundcl;
	// update channels hashlist
	int ihash = hashindex( chan->name );
	struct chanlist *hash = (struct chanlist *)malloc(sizeof(struct chanlist));
	hash->chan = chan;
	hash->next = hashlist[ihash];
	hashlist[ihash] = hash;
	//
	just_created = 1;
    }
    
    // add channel to user channels list
    cl = (struct chanlist *)malloc(sizeof(struct chanlist));
    cl->next = client->chanlist;
    cl->chan = chan;
    client->chanlist = cl;
    
    // add user to the channel
    struct clientlist *cli_list = (struct clientlist *)malloc(sizeof(struct clientlist));
    cli_list->next = chan->clientlist;
    cli_list->client = client;
    chan->clientlist = cli_list;
    chan->numusers++;

    logme(LDEBUG, "Channel: %s - Users: %ld", name, chan->numusers);
    
    return just_created;
    
} // int joinchannel


// destroy an user
// close its socket, leave all channels and free all structs 
//
int killuser(int index){

    // invalid client
    if ( client_valid(index) == 0 )
	return -1;

    logme(LDEBUG, "Killing user: %d", index);

    	
    struct sockclient *client = CLIENTS[index];
    
    // signal LUA the client disconnected
    signal_client_connection(0 /* connected */, client);
    
    // stop timer callback, if any, and free memory used
    if ( client->timer )
	ev_tm_del(NULL, client->timer);
    free( client->timer );
        
    // exit from all channels
    partallchannels(index);
    
    // remove socket libevent callback 
    ev_io_del(NULL, client->sock);
    
    // close socket
    shutdown(client->sock, SHUT_RDWR);
    close(client->sock);
    
    // free connected clients pointer
    CLIENTS[index] = NULL;

    // decrement users counter in server struct
    client->server->numclients--;
    
    // free user's struct allocated memory
    free(client);
        
    return 0;
    
} // int killuser


// perform post-fork init
//
int init()
{
    int i, h;
    
    // init threads vars
    init_threads_vars();
    
    // save launch timestamp
    timestamp_start=time(NULL);
    
    // load banslist
    load_bans_from_file();
    
    // install signal handlers
    logme(LNORMAL, "Installing signal handlers");
    signal(SIGINT, signal_handler);
    signal(SIGQUIT, signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGHUP, signal_handler);

    signal(SIGSEGV, signal_handler);
    signal(SIGILL, signal_handler);
    signal(SIGBUS, signal_handler);
    signal(SIGABRT, signal_handler);
    //signal(SIGPIPE, signal_handler);

    // ignore SIGPIPE
    struct sigaction act;
    act.sa_handler = SIG_IGN;
    sigemptyset (&act.sa_mask);
    act.sa_flags = 0;
    sigaction (SIGPIPE, &act, NULL);
    
    // setup corefile
    setup_corefile();
        
    // init libevent
    if ( ev_init(-1, EV_ADVANCED) != 0 ) {
	logme(LERROR, "Unable to init socket event library");
	return -3;
    }
    
    // init script engine
    if ( !exec_init_script() ) {
	    logme(LERROR, "Aborting");
	    return -2;
    }
    
    // for each server, create array used to hold enabled VM
    // by default, each server won't call any VM
    //
    for (i=0; i < NUMSERVERS; i++) {
	SERVERS[i]->enabled_vm_events = (char *)malloc(sizeof(char) * NUMLUAVM);
	for (h = 0; h < NUMLUAVM; h++)
	    SERVERS[i]->enabled_vm_events[h] = 0;	// don't use this VM
    }
    
    // sync temp change queue for enable_vm server's flag list
    sync_enabled_vm_temp_queue();
    
    // init channels hashlist
    for (i = 0; i < CHANS_HASHLIST; i++)
	hashlist[i] = NULL;

    return 0;
} // int init()



// main loop
//
int mainloop()
{
    
    while (exit_mainloop == 0) {
    
	// handle incoming connections
	handle_incoming_connections();

	// flush return values of threaded LUA functions, if any
	flush_threaded_return_values_list();
	
	// dispatching function for libevent
	ev_wait( NULL, MAINLOOP_SLEEP );

	// counter for LUA onmainloop trigger (if active)
	if ( lua_onmainloop_trigger_max > 0 ) {
	    ++lua_onmainloop_trigger_step;
	    // call lua onmainloop event and reset counter
	    if ( lua_onmainloop_trigger_step == lua_onmainloop_trigger_max ) {
		// call lua
		lua_onmainloop_trigger_count++;
		signal_onmainloop_event();	// script.c
		lua_onmainloop_trigger_step = 0;
	    }
	}
	
    } // main loop
    
    // if we're are, server has terminated
    stop(0);
    
    return exit_mainloop;
    
} // void mainloop


// socket event
//
static void onsockevent(void *data, int revents, int fd, struct ev_ct *ct) {

    struct sockclient *client = (struct sockclient *)data;
    int s, l, r, res, delimFound, itmp;
    char foundFlag;
    unsigned int prevclientbufflen, foundCount;
    unsigned short delimLessPacketLength = 0;
    
    // save client index because pointer could be invalidated inside LUA function (eg: client killed)
    int index = client->id;
    
    // read packet
    res = read(client->sock, &sockbuffer, MAX_RECV_BUFFER);
    
    // disconnected or data read?
    //
    if (res == -1 && ((errno == EWOULDBLOCK) || (errno == EAGAIN) || (errno == EINTR /* added - todo: check if ok */))) {
    
	// still connected
	
    } else if ( res <= 0 ) {

	// disconnected
	// TODO: this will kill the socket, ignoring data in the buffer, is it ok?
	// 
	logme(LDEBUG, "Broken connection (%d). SockID: %d - Err(%d): %s", res, index, errno, strerror(errno));
	killuser(index);
	return;
	
    } else if ( res > 0 ) {

	// something read in the socket
	// increment recv bytes counter	
	client->numrecv += res;

	// append data to client's buffer
	for (l = 0; l < res; l++) {
	    // if we reached buffer max size, log and clear buffer
	    // this means client sent a packet too large!
	    //
	    if ( client->bufferlen == MAX_RECV_BUFFER ) {
		logme(LERROR, "Protocol Error: received data packet too large. Clearing buffer. Max: %ld", MAX_RECV_BUFFER);
		client->bufferlen = 0;
		// killuser if autokilloverbuffer is iset
		if ( client->server->autokilloverbuffer == 1 )
		{
		    killuser(index);
		}
		return;
	    }
	    // append
	    client->buffer[ client->bufferlen++ ] = sockbuffer[l];
	} // append data to client's buffer

    } // something read in the socket

    // loop until we found delimitator
    //
    while(1) 
    {
	// <policy-file-request/> (23 including \0)
	// if flashcheck is enabled on this server, check if we received the
	// policy request and then add immediately our server delimitator to received buffer
	//
	if ( client->server->flashcheck == 1 && client->bufferlen > strlen(FLASH_POLICY_REQUEST) ) 
	{
	    if ( strncmp( client->buffer, FLASH_POLICY_REQUEST, strlen(FLASH_POLICY_REQUEST) ) == 0 ) 
	    {
		// signal recv line event to all scripts
		signal_line_read_write(client, client->buffer, strlen(FLASH_POLICY_REQUEST), 0 /* iswrite */);
				
		// if client became invalid, exit
		if ( !CLIENTS[index] ) return;
		
		// shift back the buffer
		for ( prevclientbufflen = client->bufferlen, client->bufferlen = 0, s = 0, r = strlen(FLASH_POLICY_REQUEST)+1; r < prevclientbufflen; client->bufferlen++, s++, r++)
		    client->buffer[s] = client->buffer[r];
	    }
	}
	
	// check min. packet len
	if ( client->server->delimitatorless == 1 )
	{
	    // delimitator-less mode: if we havent the full packet, exit from loop
	    if ( client->bufferlen < sizeof(unsigned short) ) return;
	    
	    // get packet length
	    delimLessPacketLength = MAKEWORD( client->buffer[1], client->buffer[0] );
	    
	    // packet incomplete? exit from loop
	    if ( client->bufferlen < sizeof(unsigned short) + delimLessPacketLength ) return;
	    
	    // packet complete, shift back the buffer removing the packetlength info
	    for ( prevclientbufflen = client->bufferlen, client->bufferlen = 0, s = 0, r = sizeof(unsigned short); r < prevclientbufflen; client->bufferlen++, s++, r++)
		client->buffer[s] = client->buffer[r];
	}
	else if ( client->server->delimitatorless == 0 )
	{
	    // delimitator-mode: if we haven't enough bytes yet, exit from loop
	    if ( client->bufferlen < client->server->sockdelimsize ) return;
	}
	    	
	// loop buffer (entire buffer if we're in delimitatorless mode, or bufferlen-delimisize if we're in delimitator mode)
	delimFound = -1;
	for (l = 0; l <= client->bufferlen - ( client->server->delimitatorless == 0 ? client->server->sockdelimsize : 0 ); l++) 
	{
	    
	    // search delimitator, in delimitator mode, or continue in delimitatorless mode
	    foundFlag = 0;
	    
	    if ( client->server->delimitatorless == 0 )
	    {	    	
		for (foundCount = 0, s = 0, r = l; r < l + client->server->sockdelimsize; s++, r++)
		    if ( client->buffer[r] == client->server->sockdelim[s] ) ++foundCount;
		
		if ( foundCount == client->server->sockdelimsize ) foundFlag = 1;
	    }
	    else if ( client->server->delimitatorless == 1 )
	    {
	    	if ( l == delimLessPacketLength ) foundFlag = 1;
	    }
	    
	    // delimitator found or delimitatorless mode active
	    if ( foundFlag == 1 )
	    {
		// flag used to check if line has been processed as admin command
		itmp = 0;
				
		// if it's an admin client on an administrable server, check for admin command
		if ( client->isadmin == 1 && client->server->canadmin == 1 )
		    itmp = check_admin_commands( index, client->buffer, l );
		
		// if client became invalid, exit
		if ( !CLIENTS[index] ) return;
				
		// if line has not been processed like admin command, signal recv event to LUA scripts
		if ( itmp == 0 ) 
		{
		    // signal recv line event to all scripts
		    signal_line_read_write(client, client->buffer, l, 0 /* iswrite */);

		    // if client became invalid, exit
		    if ( !CLIENTS[index] ) return;
		}
		
		delimFound = l + ( client->server->delimitatorless == 0 ? client->server->sockdelimsize : 0 );
		
		break;
		
	    } // if ( foundFlag == 1 ) - delimitator found
	    
	} // loop buffer - for (l = 0; l < client->bufferlen - client->server->sockdelimsize; l++) {
	
	// if we haven't any delimitator or client became invalid, exit
	if ( delimFound == -1 )
	    return;
	
	// else shift back the buffer
	for ( prevclientbufflen = client->bufferlen, client->bufferlen = 0, s = 0, r = delimFound; r < prevclientbufflen; client->bufferlen++, s++, r++)
	    client->buffer[s] = client->buffer[r];
								        
    } // while(1) - loop for delimitator search
    	        
} // onsockevent


// timer event
//
static void onsocktimer(void *data, struct ev_tm *tmr, struct ev_ct *ct) 
{
    
    // signal to all scripts the timer event
    struct sockclient *client = (struct sockclient *)data;
    int index = client->id;
    
    ++client->calltimes;
    signal_timer_event(client);
    
    // if client became invalid, do nothing
    if ( !CLIENTS[index] )
	return;
	
    // register again timer with same timeout
    client->timer = ev_tm_add(NULL, client->timer, client->timeout, (ev_tm_cbck_f *)onsocktimer, (void *)client);
    if ( !client->timer ) {
	logme(LERROR, "Unable to register again timer - sockid: %d - timeout: %ld", index, client->timeout);
	client->timeout = -1;
    }

} // onsocktimer


// create a server on the specified ip/port
// malloc related memory
// return server index or < 0 for errors
//
int openserver(const char *host, int port, int admin) {
        
    // try socket creation
    int sock = socket( AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in addr;
    
    if ( sock < 0 ) {
	logme(LERROR, "Unable to create socket. Err: %d", sock);
	return -1;
    }
    
    // we can reconnect to this port
    int yes = 1;
	
    if(setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int))<0) {
	logme(LERROR, "setsockopt - SO_REUSEADDR - failed - %s:%d", host, port);
	close(sock);
	return -3;
    }
    
    // set keep alive
    if(setsockopt(sock,SOL_SOCKET,SO_KEEPALIVE,&yes,sizeof(int))<0) {
	logme(LERROR, "setsockopt - SO_KEEPALIVE - failed - %s:%d", host, port);
	close(sock);
	return -3;
    }
    
    // set nonblocking socket
    if ( fcntl(sock, F_SETFL, fcntl(sock, F_GETFL, 0) | O_NONBLOCK) < 0) {
	logme(LERROR, "unable to set serversocket non blocking. %s:%d", host, port);
	close(sock);
	return -6;
    }
    
    	    
    addr.sin_family = AF_INET;
    addr.sin_port = htons( port );
	    
    // create socket and put it in listen mode
    if (strcmp(host, "*") != 0) {
        logme(LDEBUG, "Creating listening socket - %s:%d", host, port);
	// listen on specified host - resolve host
        struct hostent *hp = gethostbyname(host);
        if ( !hp) {
            logme(LERROR, "Unable to resolve host for server - %s:%d", host, port);
            close(sock);
	    return -2;
        }
        memcpy(&(addr.sin_addr.s_addr), hp->h_addr, hp->h_length);
    } else {
	// listen on all hosts
        logme(LDEBUG, "Creating listening socket - ALL:%d", port);
        addr.sin_addr.s_addr = INADDR_ANY;
    }
    
    // bind socket
    if ( bind ( sock, ( struct sockaddr *) &addr, sizeof ( addr ) ) < 0 ) {
	logme(LERROR, "Unable to bind socket. %s:%d", host, port);
	close(sock);
	return -4;
    }
    
    // finally put socket in listen mode
    if ( listen(sock,DEFAULT_LISTEN_QUEUE) < 0 ) {
	logme(LERROR, "Unable to listen on socket. %s:%d", host, port);
	close(sock);
	return -5;
    }
    
    // create server socket struct
    SERVERS = (struct sockserv **)realloc(SERVERS, sizeof(struct sockserv *) * (NUMSERVERS + 1));
    SERVERS[NUMSERVERS]=(struct sockserv *)malloc(sizeof(struct sockserv));
    
    SERVERS[NUMSERVERS]->sock = sock;
    SERVERS[NUMSERVERS]->start = time(NULL);
    SERVERS[NUMSERVERS]->lastjoin = 0;
    SERVERS[NUMSERVERS]->numclients = 0;
    SERVERS[NUMSERVERS]->delimitatorless = 0;
    SERVERS[NUMSERVERS]->listen_port = port;
    SERVERS[NUMSERVERS]->canadmin = (char)admin;
    SERVERS[NUMSERVERS]->id = NUMSERVERS;
    SERVERS[NUMSERVERS]->autodelim = 1;	// by default, we automatically send delimitator after each send
    SERVERS[NUMSERVERS]->flashcheck = 0; // by default, flashcheck policy file requests is not enabled
    SERVERS[NUMSERVERS]->autokilloverbuffer = 1; // by default, autokill flag for clients which sends packets too large is enabled
    strncpy2( SERVERS[NUMSERVERS]->sockdelim, DEFAULT_SOCKDELIM, SIZE_SOCKDELIM);	// default delimitator \r\n
    SERVERS[NUMSERVERS]->sockdelimsize = DEFAULT_SOCKDELIM_SIZE;			// default delimitator size (2)
    
    strncpy2( SERVERS[NUMSERVERS]->listen_address, host, MAX_SERVER_ADDRESS_LEN + 1 );
    memcpy(&(SERVERS[NUMSERVERS]->addr), &addr, sizeof(struct sockaddr_in) );
    
    ++NUMSERVERS;
    
    // ok, return new server socket index
    return NUMSERVERS - 1;
} // int openserver


// part specified client from all channels
//
int partallchannels(int index) {

    // invalid client
    if ( client_valid(index) == 0 )
	return -1;

    int numremoved = 0;
    struct sockclient *client = CLIENTS[index];
    struct chanlist *cl = client->chanlist;
    // loop through all user's channels and part from them
    while (cl) {
	partchannel(index, cl->chan->name);
	++numremoved;
	cl = cl->next;
    }
    return numremoved;    
    
} // partallchannels


// part specified client from specified channel
//
int partchannel(int index, const char *name) {

    // invalid client
    if ( client_valid(index) == 0 )
	return -1;

    // check if user is in channel
    struct sockclient *client = CLIENTS[index];
    struct chanlist *prevcl, *cl = client->chanlist;
    struct channel *chan = NULL;
    
    while (cl) {
	// channel found
	if (strcmp(cl->chan->name, name) == 0) {
	    chan = cl->chan;
	    break;
	}
	cl = cl->next;
    }

    // user is not in specified channel
    if ( !chan )
	return -2;

    // we have to remove channel from client's channels list
    prevcl = NULL;
    cl = client->chanlist;
    while (cl) {
	// channel found
	if ( cl->chan == chan ) {
	    if (prevcl)	// middle rem A->[B]->C
		prevcl->next = cl->next;
	    else	// head rem [A]->B->C
		client->chanlist = cl->next;
	    free(cl);
	    //logme(LDEBUG, "Client: %d - Removed from: %s", index, chan->name);
	    break;
	}    
	prevcl = cl;
	cl = cl->next;
    }
    
    unsigned int numusers = -1;
    
    // we have to remove client from channel's users list
    struct clientlist *prevccl = NULL, *ccl = chan->clientlist;
    while (ccl) {
	if (ccl->client == client) {
	    if (prevccl)	// middle rem A->[B]->C
		prevccl->next = ccl->next;
	    else		// head rem [A]->B->C
		chan->clientlist = ccl->next;
	    free(ccl);
	    
	    --chan->numusers;
	    numusers = chan->numusers;
	    
	    logme(LDEBUG, "Client: %d - Removed from: %s - Users: %d", index, chan->name, numusers);
	    break;
	}
	prevccl = ccl;
	ccl = ccl->next;
    }
    
    
    // if channel is now empty, delete it
    if ( numusers == 0 ) {
    
	// remove channel from channels hash list
	int ihash = hashindex( chan->name );
	prevcl = NULL;
	cl = hashlist[ ihash ];
	while (cl) {
	    // channel found in hashlist
	    if (cl->chan == chan) {
		if (prevcl)	// middle rem A->[B]->C
		    prevcl->next = cl->next;
		else	// head rem [A]->B->C
		    hashlist[ ihash ] = cl->next;
		// free hashlist item
		free(cl);		    		
		break;
	    }
	    prevcl = cl;
	    cl = cl->next;
	} // remove channel from channels hashlist
	
	
	// remove channel from channels list
	prevcl = NULL;
	cl = chanlist;
	while (cl) {
	    // channel found
	    if ( cl->chan == chan ) {
		if (prevcl)	// middle rem A->[B]->C
		    prevcl->next = cl->next;
		else	// head rem [A]->B->C
		    chanlist = cl->next;
		    
		logme(LDEBUG, "Removed channel: %s", chan->name);
		
		// free channel struct
		free(cl->chan);
		// free channel link item from channels list
		free(cl);
		
		break;
	    }    
	    prevcl = cl;
	    cl = cl->next;
	}
	
    } // no users left in channel, remove it
    
    return numusers;
} // partchannel


// ask for a reboot(2), shutdown(1)
//
void rebootshutdown(int action) {
    switch (action) {
	case 1:	// shutdown
	    exit_mainloop = 1;
	    logme(LNORMAL, "Server mode set to: shutdown");
	break;
	case 2:	// reboot
	    exit_mainloop = 2;
	    logme(LNORMAL, "Server mode set to: reboot");
	break;
    }; // switch action
} // rebootshutdown


// set admin flag to specified client
//
int set_admin_client(int index, int admin) {

    // invalid client
    if ( client_valid(index) == 0 )
	return -1;
	
    CLIENTS[index]->isadmin = (char)admin;
    
    logme(LDEBUG, "Admin flag for client: %d - Now set: %d", index, admin);
    
    return 1;
} // set_admin_client


// set enabled_vm flag on specified server
// sanity check on servindex
// vmindex must be a valid VM index
//
int set_enabled_vm_server(int servindex, int vmindex, int flag) {

    // server index not valid
    if (NUMSERVERS == 0 || servindex >= NUMSERVERS || servindex < 0 || !SERVERS[servindex]) {
	logme(LERROR, "EnableVMServer: ServIndex: %d - Not valid", servindex);
	return -1;
    }

    SERVERS[servindex]->enabled_vm_events[vmindex] = (flag == 1 ? 1 : 0);
    
    return 0;    
} // set_enabledvm_server


// set or get autodelim flag for specified server
// if newflag = -1, we won't change flag (used to get value)
// the function always return the flag for specified server (or -1 if error)
//
int set_get_autodelim(int servindex, int newflag) {

    // server index not valid
    if (NUMSERVERS == 0 || servindex >= NUMSERVERS || servindex < 0 || !SERVERS[servindex]) {
	logme(LERROR, "AutoDelim: ServIndex: %d - Not valid", servindex);
	return -1;
    }
    
    // set
    if ( newflag > -1 ) {
	switch (newflag) {
	    case 0:
		logme(LDEBUG, "AutoDelim - Server: %d - Disabled", servindex);
		SERVERS[servindex]->autodelim = 0;
	    break;
	    case 1:
	    default:
		logme(LDEBUG, "AutoDelim - Server: %d - Enabled", servindex);
		SERVERS[servindex]->autodelim = 1;
	};
    }
    
    // get
    return SERVERS[servindex]->autodelim;
    
} // set_get_autodelim


// set or get flashcheck flag for specified server
// flashcheck will tell to daemon if we must check for flash policy file requests
// and trigger events if it is found
//
int set_get_flashcheck(int servindex, int newflag) {

    // server index not valid
    if (NUMSERVERS == 0 || servindex >= NUMSERVERS || servindex < 0 || !SERVERS[servindex]) {
	logme(LERROR, "FlashCheck: ServIndex: %d - Not valid", servindex);
	return -1;
    }
    
    // set
    if ( newflag > -1 ) {
	switch (newflag) {
	    case 0:
		logme(LDEBUG, "FlashCheck - Server: %d - Disabled", servindex);
		SERVERS[servindex]->flashcheck = 0;
	    break;
	    case 1:
	    default:
		logme(LDEBUG, "FlashCheck - Server: %d - Enabled", servindex);
		SERVERS[servindex]->flashcheck = 1;
	};
    }
    
    // get
    return SERVERS[servindex]->flashcheck;
    
} // set_get_flashcheck


// set or get autokill flag for clients which sends packets too large
//
int set_get_autokilloverbuffer(int servindex, int newflag) {

    // server index not valid
    if (NUMSERVERS == 0 || servindex >= NUMSERVERS || servindex < 0 || !SERVERS[servindex]) {
	logme(LERROR, "AutoKillOverBuffer: ServIndex: %d - Not valid", servindex);
	return -1;
    }
    
    // set
    if ( newflag > -1 ) {
	switch (newflag) {
	    case 0:
		logme(LDEBUG, "AutoKillOverBuffer - Server: %d - Disabled", servindex);
		SERVERS[servindex]->autokilloverbuffer = 0;
	    break;
	    case 1:
	    default:
		logme(LDEBUG, "AutoKillOverBuffer - Server: %d - Enabled", servindex);
		SERVERS[servindex]->autokilloverbuffer = 1;
	};
    }
    
    // get
    return SERVERS[servindex]->autokilloverbuffer;
    
} // set_get_autokilloverbuffer


// set or get user-defined clients numeric flag
// if set = 1 we want to set value
// the function always returns the flag for specified client
//
long set_get_userflag(char set, int index, long newflag) {

    // invalid client
    if ( client_valid(index) == 0 )
	return -1;
    
    // set
    if ( set == 1 )
	CLIENTS[index]->flag1 = newflag;
    
    // get
    return CLIENTS[index]->flag1;
    
} // set_get_userflag


// set a new delimitator for specified server socket
//
int set_new_delimitator(int servindex, const char *delim, int delimlen) {

    // server index not valid
    if (NUMSERVERS == 0 || servindex >= NUMSERVERS || servindex < 0  || !SERVERS[servindex]) {
	logme(LERROR, "ServIndex: %d - Not valid", servindex);
	return -1;
    }
    
    // absolute real delimitator length
    int adelimlen = abs(delimlen);
    
    // delimitator too long
    if ( adelimlen > SIZE_SOCKDELIM )
	return -2;

    // save delimitator data
    int i;
    for (i = 0; i < adelimlen; i++)
	SERVERS[servindex]->sockdelim[i] = delim[i];
    
    // save delimitator size
    SERVERS[servindex]->sockdelimsize = i;
    
    // save delimitatorless flag, if delimlen is negative
    SERVERS[servindex]->delimitatorless = ( delimlen <= 0 ? 1 : 0 );
    
    return i;
} // int set_new_delimitator


// set or remove a timer event for specified client
// 
int settimer(int index, long timeout)
{

    // invalid client
    if ( client_valid(index) == 0 )
	return -1;
	
    struct sockclient *client = CLIENTS[index];
    
    // remove previously added timer, if any
    //
    if ( client->timer ) {
	ev_tm_del(NULL, client->timer);
	free(client->timer);
	client->timer = NULL;
	client->timeout = -1;
    }
    
    // we want to set a new timer
    if ( timeout >= 0 ) {
		
	// add timer, use existing one if any, or create a new one
	client->timer = ev_tm_add(NULL, client->timer, timeout, (ev_tm_cbck_f *)onsocktimer, (void *)client);
	if ( client->timer ) {
	    client->timeout = timeout;
	    client->calltimes = 0;
	} else {
	    logme(LERROR, "Unable to register timer - sockid: %d - timeout: %ld", client->id, timeout);
	    client->timeout = -1;
	    return -2;
	}
  	
    }
    
    // ok
    return 1;
} // settimer


//#define SU_USER "nobody"

/* ripped this out of hybrid7 out of lazyness. */
void setup_corefile()
{
#if 0
    struct rlimit corelimit;
    struct passwd *pw = NULL;
    char          *cp = NULL;

    /* if switch to nobody failed */
    if (NULL == (pw = getpwnam(SU_USER))) {
        fprintf(stderr, "Cannot get uid from user(%s). Error: %s\n",
                         SU_USER, strerror(errno));
        return -1;
    }

    /* try to switch to nobody */
    if ((setuid(pw->pw_uid) < 0) || (seteuid(pw->pw_uid) < 0))
    {
        fprintf(stderr, "Cannot switch to user(%s). Error: %s\n",
                         SU_USER, strerror(errno));
        return -2;
    }
#endif
// disable corefile on mac
#ifndef __APPLE__
    struct rlimit rlim; /* resource limits */

    /* force to make coredump */
    if (prctl(PR_SET_DUMPABLE, 1) < 0)
    {
	logme(LERROR, "Cannot enable core dumping. Error: %s", strerror(errno));
        return;
    }
   
    /* Set corefilesize to maximum */
    if (!getrlimit(RLIMIT_CORE, &rlim))
    {
	rlim.rlim_cur = rlim.rlim_max;
	setrlimit(RLIMIT_CORE, &rlim);
    }
#endif
} // setup_corefile


// signal handler
//
void signal_handler(int sig) {
    
    logme(LNORMAL, "Got signal: %d", sig);
    
    switch(sig) {
	// normal signals, safely quit application
	case SIGTERM:
	case SIGINT:
	case SIGQUIT:
	case SIGHUP:
	    signal(sig, signal_handler);
	    stop(0);
	    exit(sig);
	break;
	
	// fatal errors
	// take default action, so core will be dumped (if possible)
	case SIGSEGV:
	case SIGILL:
	case SIGBUS:
	case SIGABRT:
	//case SIGPIPE:
	    //stop(sig);
	    //exit(sig);
	    signal(sig, SIG_DFL);
	break;
    }; // switch (sig)
    
} // signal_handler


// stop
// called on daemon exit, cleanup/free/dealloc etc
//
void stop(int sig) {
    
    // clear list for return values of threaded lua functions
    free_threads_vars();
    
    // free bans list
    clear_bans();
    
    // kill all users
    int i;
    if (CLIENTSMAX > 0)
        for (i = 0; i < CLIENTSMAX; i++)
	    killuser(i);

    // free clients array
    free(CLIENTS);	CLIENTS = NULL;
        
    // free libevent
    ev_free();
    
    // free scripting engine
    end_script_engine();

    // sanity check: if users kill is ok, all channels hashlist should be empty here
    for (i=0; i < CHANS_HASHLIST; i++)
	if (hashlist[i])
	    logme(LERROR, "Memory corruption. Hashlist: %d is not void!", i);
    
    // free all socket servers and structs used by them
    for (i = 0; i < NUMSERVERS; i++) {
	close(SERVERS[i]->sock);
	free(SERVERS[i]->enabled_vm_events);
	free(SERVERS[i]);
    }
    free(SERVERS);
    NUMSERVERS=0;
    
    // free throttle list
    clear_throttling();

    // close logfile
    if (logfp)
	fclose(logfp);
    
} // void stop()
