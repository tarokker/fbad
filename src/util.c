/*
 Copyright (c) 2011, Daniele Maiorana <tarokker@tiscali.it>
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.
    * Neither the name of the creator nor the names of its contributors may be used to endorse or promote products derived from this software 
without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/

#include "version.h"
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "struct.h"
#include "extern.h"
#include "defines.h"
#include <time.h>

#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <pthread.h>

#define LOGBUFFSIZE 2048

// forward declarations
static inline int _match(const char *wild, const char *string);

// current scripts path
char CURRENT_SCRIPTS_PATH[255];

// log file
FILE *logfp = NULL;

// log filename (current and new)
char logfname1[256], logfname2[256];	

// buffers used for log file
static char _logbuffer[LOGBUFFSIZE+1], _logbuffer2[LOGBUFFSIZE+1];


#define MAX_CALLS 512
static int  calls = 0;


// calculate crc32 value for specified buffer
//
unsigned long crc32( const void *buf, int bufLen )
{
    static const unsigned long crcTable[256] = {
    0x00000000,0x77073096,0xEE0E612C,0x990951BA,0x076DC419,0x706AF48F,0xE963A535,
    0x9E6495A3,0x0EDB8832,0x79DCB8A4,0xE0D5E91E,0x97D2D988,0x09B64C2B,0x7EB17CBD,
    0xE7B82D07,0x90BF1D91,0x1DB71064,0x6AB020F2,0xF3B97148,0x84BE41DE,0x1ADAD47D,
    0x6DDDE4EB,0xF4D4B551,0x83D385C7,0x136C9856,0x646BA8C0,0xFD62F97A,0x8A65C9EC,
    0x14015C4F,0x63066CD9,0xFA0F3D63,0x8D080DF5,0x3B6E20C8,0x4C69105E,0xD56041E4,
    0xA2677172,0x3C03E4D1,0x4B04D447,0xD20D85FD,0xA50AB56B,0x35B5A8FA,0x42B2986C,
    0xDBBBC9D6,0xACBCF940,0x32D86CE3,0x45DF5C75,0xDCD60DCF,0xABD13D59,0x26D930AC,
    0x51DE003A,0xC8D75180,0xBFD06116,0x21B4F4B5,0x56B3C423,0xCFBA9599,0xB8BDA50F,
    0x2802B89E,0x5F058808,0xC60CD9B2,0xB10BE924,0x2F6F7C87,0x58684C11,0xC1611DAB,
    0xB6662D3D,0x76DC4190,0x01DB7106,0x98D220BC,0xEFD5102A,0x71B18589,0x06B6B51F,
    0x9FBFE4A5,0xE8B8D433,0x7807C9A2,0x0F00F934,0x9609A88E,0xE10E9818,0x7F6A0DBB,
    0x086D3D2D,0x91646C97,0xE6635C01,0x6B6B51F4,0x1C6C6162,0x856530D8,0xF262004E,
    0x6C0695ED,0x1B01A57B,0x8208F4C1,0xF50FC457,0x65B0D9C6,0x12B7E950,0x8BBEB8EA,
    0xFCB9887C,0x62DD1DDF,0x15DA2D49,0x8CD37CF3,0xFBD44C65,0x4DB26158,0x3AB551CE,
    0xA3BC0074,0xD4BB30E2,0x4ADFA541,0x3DD895D7,0xA4D1C46D,0xD3D6F4FB,0x4369E96A,
    0x346ED9FC,0xAD678846,0xDA60B8D0,0x44042D73,0x33031DE5,0xAA0A4C5F,0xDD0D7CC9,
    0x5005713C,0x270241AA,0xBE0B1010,0xC90C2086,0x5768B525,0x206F85B3,0xB966D409,
    0xCE61E49F,0x5EDEF90E,0x29D9C998,0xB0D09822,0xC7D7A8B4,0x59B33D17,0x2EB40D81,
    0xB7BD5C3B,0xC0BA6CAD,0xEDB88320,0x9ABFB3B6,0x03B6E20C,0x74B1D29A,0xEAD54739,
    0x9DD277AF,0x04DB2615,0x73DC1683,0xE3630B12,0x94643B84,0x0D6D6A3E,0x7A6A5AA8,
    0xE40ECF0B,0x9309FF9D,0x0A00AE27,0x7D079EB1,0xF00F9344,0x8708A3D2,0x1E01F268,
    0x6906C2FE,0xF762575D,0x806567CB,0x196C3671,0x6E6B06E7,0xFED41B76,0x89D32BE0,
    0x10DA7A5A,0x67DD4ACC,0xF9B9DF6F,0x8EBEEFF9,0x17B7BE43,0x60B08ED5,0xD6D6A3E8,
    0xA1D1937E,0x38D8C2C4,0x4FDFF252,0xD1BB67F1,0xA6BC5767,0x3FB506DD,0x48B2364B,
    0xD80D2BDA,0xAF0A1B4C,0x36034AF6,0x41047A60,0xDF60EFC3,0xA867DF55,0x316E8EEF,
    0x4669BE79,0xCB61B38C,0xBC66831A,0x256FD2A0,0x5268E236,0xCC0C7795,0xBB0B4703,
    0x220216B9,0x5505262F,0xC5BA3BBE,0xB2BD0B28,0x2BB45A92,0x5CB36A04,0xC2D7FFA7,
    0xB5D0CF31,0x2CD99E8B,0x5BDEAE1D,0x9B64C2B0,0xEC63F226,0x756AA39C,0x026D930A,
    0x9C0906A9,0xEB0E363F,0x72076785,0x05005713,0x95BF4A82,0xE2B87A14,0x7BB12BAE,
    0x0CB61B38,0x92D28E9B,0xE5D5BE0D,0x7CDCEFB7,0x0BDBDF21,0x86D3D2D4,0xF1D4E242,
    0x68DDB3F8,0x1FDA836E,0x81BE16CD,0xF6B9265B,0x6FB077E1,0x18B74777,0x88085AE6,
    0xFF0F6A70,0x66063BCA,0x11010B5C,0x8F659EFF,0xF862AE69,0x616BFFD3,0x166CCF45,
    0xA00AE278,0xD70DD2EE,0x4E048354,0x3903B3C2,0xA7672661,0xD06016F7,0x4969474D,
    0x3E6E77DB,0xAED16A4A,0xD9D65ADC,0x40DF0B66,0x37D83BF0,0xA9BCAE53,0xDEBB9EC5,
    0x47B2CF7F,0x30B5FFE9,0xBDBDF21C,0xCABAC28A,0x53B39330,0x24B4A3A6,0xBAD03605,
    0xCDD70693,0x54DE5729,0x23D967BF,0xB3667A2E,0xC4614AB8,0x5D681B02,0x2A6F2B94,
    0xB40BBE37,0xC30C8EA1,0x5A05DF1B,0x2D02EF8D };
    unsigned long crc32;
    unsigned char *byteBuf;
    int i;
    
    crc32 = 0 /*inCrc32*/ ^ 0xFFFFFFFF;
    byteBuf = (unsigned char*) buf;
    for (i=0; i < bufLen; i++) {
    	crc32 = (crc32 >> 8) ^ crcTable[ (crc32 ^ byteBuf[i]) & 0xFF ];
    }
    return( crc32 ^ 0xFFFFFFFF );
}

static inline int _match(const char *wild, const char *string) {
    if (calls++ > MAX_CALLS)
        return 1;

    while(1) {
        if(!*wild)
            return ((!*string) ? 0 : 1);
        if(*wild=='*') {
            wild++;
            while(*wild=='*')
                wild++;
            while(*wild=='?' && *string)
                wild++;string++;
            if(!*wild)
                return 0;
            if(*wild=='*')
                continue;
            while(*string) {
                if(toupper(*string) == toupper(*wild) && !_match((wild+1), (string+1)))
                    return 0;
                string++;
            }
        }
        if(!*string)
            return 1;
        if(*wild!='?' && toupper(*string)!= toupper(*wild))
    	    return 1;
        string++;
        wild++;
    }
    return 1;
} // static inline int _match(char *wild, char *string)



// log on file/stdout
//
int logme(int level, const char *pattern, ...)
{
    if ( cmdline.loglevel < level)
	return 1;	// lower log level
    
    // build string
    va_list      vl;
    va_start(vl, pattern);
    (void) vsnprintf(_logbuffer, LOGBUFFSIZE, pattern, vl);
    va_end(vl);

    // time
    time_t curtime = time(NULL);
    struct tm *loctime = localtime(&curtime);
    
    // build log line
    snprintf(_logbuffer2, LOGBUFFSIZE, "[%02d/%02d/%04d - %02d:%02d:%02d] - [%06d:%02d] %s\n", loctime->tm_mday, loctime->tm_mon + 1, 1900+loctime->tm_year, 
	loctime->tm_hour, loctime->tm_min, loctime->tm_sec, (pid<0?0:pid), level, _logbuffer);
    
    // if we're main process, we can also make output on stdout
    if ( pid != 0 )
	printf("%s", _logbuffer2);
    
    // calculate new log filename according to rotate rules
    switch (cmdline.logrotate) {
	case LROTDAY:	// rotate each day
	    snprintf( logfname2, 255, "%sdaemon_%04d_%02d_%02d.log", ROOT_LOGFILES, loctime->tm_year + 1900, loctime->tm_mon + 1, loctime->tm_mday);
	break;
	case LROTMONTH:	// rotate each month
	    snprintf( logfname2, 255, "%sdaemon_%04d_%02d.log", ROOT_LOGFILES, loctime->tm_year + 1900, loctime->tm_mon + 1);
	break;
	case LROTYEAR:	// rotate each year
	    snprintf( logfname2, 255, "%sdaemon_%04d.log", ROOT_LOGFILES, loctime->tm_year + 1900);
	break;
	default:	// default file name, never rotate
	    snprintf( logfname2, 255, "%sdaemon.log", ROOT_LOGFILES);
    }
    
    // log on file
    // open logfile if needed
    pthread_mutex_lock( &mutex_logfile );

    // we have to open file yet, open it and use new generated log filename
    if ( !logfp ) {
	strcpy(logfname1, logfname2);
	logfp = fopen(logfname1, "a");
    } else {
	// file is already opened, check if logname has changed
	// if filename changed, close old log and open new one
	if ( strcmp(logfname1, logfname2) != 0 ) {
	    fclose(logfp);
	    strcpy(logfname1, logfname2);
	    logfp = fopen(logfname1, "a");
	}
    }
    
    if ( logfp ) {
	fputs(_logbuffer2, logfp);
	fflush(logfp);
	//fclose(fp);
    } else {
	// can we still raise error?
	if ( pid != 0 )	// padre
	    printf("Unable to write logfile: %s", logfname1);
    }
        
    pthread_mutex_unlock( &mutex_logfile );
    
    return 0;
} // int logme


// perform a strings matching using a wildcard pattern
//
int match(const char *n, const char *m) {
    calls=0;
    return _match(n,m);
}

// perform command line parsing
//
char parse_command(int argc, char *argv[]) {
    int c;
    char unknown = 0;
    int search_second_param = 0;	// searching second param
    
    // command line defaults
    cmdline.help = 0;
    cmdline.nodaemon = 0;
    cmdline.loglevel = DEFAULT_LOG_LEVEL;
    cmdline.logrotate = LROTDAY;
    
    if ( argc == 1 )
	return 1;	// no params

    // parse arguments
    for (c=1; c < argc; c++) {

	if ( strcmp(argv[c], "--help") == 0 )			// help
	    cmdline.help = 1;
	else if ( strcmp(argv[c], "--single") == 0 )		// no daemon
	    cmdline.nodaemon = 1;
	else if ( strcmp(argv[c], "--scripts") == 0 )		// scripts path following
	    search_second_param = 1;				// search_second_param -> 1 -> scripts path
	else if ( strcmp(argv[c], "--d0") == 0 )		// log level 0
	    cmdline.loglevel = LERROR;
	else if ( strcmp(argv[c], "--d1") == 0 )		// log level 1
	    cmdline.loglevel = LWARNING;
	else if ( strcmp(argv[c], "--d2") == 0 )		// log level 2
	    cmdline.loglevel = LNORMAL;
	else if ( strcmp(argv[c], "--d3") == 0 )		// log level 3
	    cmdline.loglevel = LNOTICE;
	else if ( strcmp(argv[c], "--d4") == 0 )		// log level 4
	    cmdline.loglevel = LDEBUG;
	else if ( strcmp(argv[c], "--d5") == 0 )		// log level 5
	    cmdline.loglevel = LVERYDEBUG;
	else if ( strcmp(argv[c], "--rn") == 0 )		// log rotate: none
	    cmdline.logrotate = LROTNONE;
	else if ( strcmp(argv[c], "--rd") == 0 )		// log rotate: day
	    cmdline.logrotate = LROTDAY;
	else if ( strcmp(argv[c], "--rm") == 0 )		// log rotate: month
	    cmdline.logrotate = LROTMONTH;
	else if ( strcmp(argv[c], "--ry") == 0 )		// log rotate: year
	    cmdline.logrotate = LROTYEAR;
	else {							// second param or unknown command
	    switch ( search_second_param ) {
		case 1:			// scripts path
		    strncpy2( CURRENT_SCRIPTS_PATH, argv[c], 255 );
		break;
		default:
		    unknown = 1;
	    }; // switch ( search_second_param ) {
	} // second param or unknown command
	
    } // for (c=0; c < argc; c++) {
    
    // show help if user asked for unknown options
    return ( unknown == 0 ? 1 : 0 );
    
} // parse_command


// print help
//
void print_help() {
    printf("\n");
    printf("%s %s\n", VERSION_FULL, VERSION_NUMBER);
    printf("\n");
    printf("Available options:\n");
    printf("\t--single\t\t\tdisable daemon mode\n");
    printf("\t--scripts path\t\t\tchange scripts base path (default scripts/)\n");
    printf("\t--help\t\t\t\tshows this help\n");
    printf("\t--d0[5]\t\t\t\tlog level (default %d)\n", DEFAULT_LOG_LEVEL);
    printf("\t--r[n,d,m,y]\t\t\tlogs rotation by none,day,month or year (default: day)\n");
    printf("\n");
} // void print_help


// crypt a string using rc4
//
int rc4(const char * source, int len, const char * pwd, char *dest){
        
    int i,j=0,t,tmp,tmp2,s[256], k[256];
	
    // build array with keys
    for (tmp=0;tmp<256;tmp++){
	s[tmp]=tmp;
        k[tmp]=pwd[(tmp % strlen((char *)pwd))];
    }
    for (i=0;i<256;i++){
	j = (j + s[i] + k[i]) % 256;
        tmp=s[i];
        s[i]=s[j];
        s[j]=tmp;
    }
    
    // perform crypt
    i=j=0;
    for (tmp=0;tmp<len;tmp++){
    
        i = (i + 1) % 256;
        j = (j + s[i]) % 256;

        tmp2=s[i];
        s[i]=s[j];
        s[j]=tmp2;
        t = (s[i] + s[j]) % 256;

	if (s[t]==source[tmp])
    	    dest[tmp]=source[tmp];
	else
    	    dest[tmp]=s[t]^source[tmp];
    }
    //temp[tmp]='\0';
    
    return len;
} // rc4


// attempt to make a case insensitive strcmp :)
//
int stricmp(const char *s1, const char *s2) {
    
    if (!s1 || !s2)			return -1;	// sanity
    if ( strlen(s1) < strlen(s2) )	return -1;
    if ( strlen(s1) > strlen(s2) )	return -1;
    
    int a;
    for (a=0; a < strlen(s1); a++) {
	if ( toupper(s1[a]) == toupper(s2[a]))
	    continue;
	else
	    return ( toupper(s1[a]) < toupper(s2[a]) ? -1 : 1 );
    }
    return 0;	// same string
} // stricmp

// same as strncpy but:
// it checks if n >= strlen(src) and add \0 to fix strncpy behaviour
//
char *strncpy2(char *dest, const char *src, size_t n) {
    
    char *res = strncpy(dest, src, n);
    
    if ( !res ) return NULL;
    
    // add missing \0 if strncpy didn't add it !
    if ( n >= strlen(src) ) 
	dest[n-1]='\0';
	
    return dest;
    
} // strncpy2
