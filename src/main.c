/*
 Copyright (c) 2011, Daniele Maiorana <tarokker@tiscali.it>
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.
    * Neither the name of the creator nor the names of its contributors may be used to endorse or promote products derived from this software 
without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h> 

#include "defines.h"
#include "extern.h"
#include "struct.h"
#include "version.h"

			
int main(int argc, char*argv[]) {    
    
    // save default scripts path
    strncpy2( CURRENT_SCRIPTS_PATH, DEFAULT_SCRIPTSPATH, 255 );
	    
    // command line parsing
    if ( !parse_command(argc, argv) || cmdline.help ) {
	print_help();
	return 1;
    }
    
    // make the process fork() if requested, then exit
    if ( !cmdline.nodaemon ) {
	pid = fork();
	if ( pid < 0 ) {
	    logme(LERROR, "Unable to fork process. Aborting.");
	    return -9;
	} else if ( pid > 0 ) {
	    logme(LNORMAL, "%s %s", VERSION_FULL, VERSION_STRBUILD);
	    logme(LNORMAL, "Now becoming a daemon. PID: %ld. Log level: %d", pid, cmdline.loglevel);
	    return 0;
	}
    } else {
	logme(LNORMAL, "%s %s", VERSION_FULL, VERSION_STRBUILD);
	logme(LNORMAL, "Now starting in single mode. Log level: %d", cmdline.loglevel);
    }

    
    // post-fork init
    int res = init();
        
    if ( res < 0 ) // errore
	return res;

    // the main loop
    res = mainloop();
    
    if ( res == 2 )	// restart requested
	execv(argv[0], argv);
	
    return 0;    
} // int main
