/*
 Copyright (c) 2011, Daniele Maiorana <tarokker@tiscali.it>
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.
    * Neither the name of the creator nor the names of its contributors may be used to endorse or promote products derived from this software 
without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STRUCT_INCLUDE_H
#define STRUCT_INCLUDE_H

#include "defines.h"
#include <event.h>
#include <netinet/in.h>
#include <lua.h>
#include <lauxlib.h>

// struct which holds info about each LUA VM
//
struct LUA {
    lua_State *lua;
    char *filename;
};

// this struct will temporary queue values to be changed on servers for enabled_vm flag
// (filled before loading config)
struct enabled_vm_temp_queue {
    int servindex;
    char *script;
    char enabled;
    struct enabled_vm_temp_queue *next;
};

// command line parsing struct
struct _cmdline {
    char help;
    char nodaemon;
    int loglevel;
    char logrotate;
} cmdline;

// channel struct
struct channel {
    char name[MAX_CHANNEL_NAME+1];	// nome canale
    char pass[MAX_CHAN_PASSWORD+1];	// password canale
    unsigned int numusers;		// quanti utenti ci sono dentro
    time_t created_time;		// quando e' stato creato
    struct clientlist *clientlist;	// lista dei client dentro il canale
};

// channels list
struct chanlist {
    struct channel *chan;	// puntatore al canale
    struct chanlist *next;	// prossimo canale
};
struct chanlist *chanlist;

// clients sockets list
struct clientlist {
    struct sockclient *client;
    struct clientlist *next;
};


// server socket
struct sockserv {
    int sock;           	// socket descriptor
    int id;			// index in SERVERS array
    time_t start;		// when we started server
    time_t lastjoin;		// ts of last client's join
    unsigned long numclients;	// how many clients are connected
    struct sockaddr_in addr;    // socket struct
    int listen_port;					// port
    char listen_address[MAX_SERVER_ADDRESS_LEN+1];	// listen host
    unsigned char canadmin;				// users can send admin commands on this server
    char *enabled_vm_events;	// this array holds indexes of enabled VMs on this server (events with a client index, will be called, eg: onwrite, onread etc)
				// enabled_vm_events[1] = 1, means VM:1 will receive events from this server
    char sockdelim[SIZE_SOCKDELIM];	// packets delimitator used by this server
    unsigned char sockdelimsize;	// size in bytes of current delimitator
    char autodelim;			// autosend delimitator after each send (default: 1, yes)
    char autokilloverbuffer; 		// autokill flag for clients which sends packets too large (default 1)
    char delimitatorless;		// delimitatorless mode (default: 0) - see setdelim lua config command
    char flashcheck;			// flash policy file autocheck (default: 0, no)
};

// struct client socket
struct sockclient {
    int sock;           // socket descriptor
    int id;		// index in CLIENTS array
    time_t tsjoined;	// join timestamp
    struct sockserv *server;	// on which server this client is connected
    struct sockaddr_in addr;    // socket struct
    char buffer[MAX_RECV_BUFFER+1];	// buffer used for incoming packets
    unsigned int bufferlen;		// received buffer len
    struct chanlist *chanlist;		// channels list on which the used entered
    char isadmin;			// this client is admin?
    unsigned long numrecv, numsend;	// bytes sent/recv
    
    struct ev_tm *timer;			// timer struct used by libevent class in order to raise timer events
    long timeout;			// timeout for timer events (default = -1, don't raise timer event)
    unsigned long calltimes;		// how many times the timer event has been raised
    char ip[MAX_IP_LEN+1];		// client ip address
    long flag1;				// user-defined flag (default:0)
};

// throttled ip list
struct throttle_list {
    char *ip;			// ip
    time_t tslast;		// last connection timestamp
    unsigned int throttle;	// how many seconds the ip should be throttled (0 = it isn't throttled)
    unsigned int numjoin;	// how many times he joined from last tslast
    struct throttle_list *next;	// next item in list
};

// banned ip list
struct bans_list {
    char *ip;	// ip
    char *reason;	// ban reason
    time_t tslast;	// timestamp of last join retry
    unsigned long numtriggers;	// how many times ban has been triggered
    struct bans_list *next;	// next item in list
};


// parameters of thread function called by 'thread' LUA func
struct pars_thread_newlua_func {
    lua_State *lua;	// pointer to new created lua VM
    lua_State *luaorig;	// pointer to lua VM which created the new one
    int inpars;		// number of input params of 'run' function
};


// struct which holds return values of a threaded LUA func execution
struct return_thread_newlua_func {
    char *data;
    lua_State *luaorig;		// pointer to lua VM which created the threaded one
    unsigned int datalen;
    struct return_thread_newlua_func *next;
};

#endif // STRUCT_INCLUDE_H
