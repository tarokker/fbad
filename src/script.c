/*
 Copyright (c) 2011, Daniele Maiorana <tarokker@tiscali.it>
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.
    * Neither the name of the creator nor the names of its contributors may be used to endorse or promote products derived from this software 
without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/

#include "defines.h"
#include "extern.h"
#include "struct.h"

#include <dirent.h>
#include <ctype.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h> 

#ifdef HAVE_LIBREADLINE
#include <readline/readline.h>
#endif

// lua VMs array
struct LUA **LUA = NULL;
int NUMLUAVM = 0;

// config.lua has been loaded
static char config_loaded = 0;

// temporary queue used before loading config.lua for servers enabled_vm flags items to be changed
struct enabled_vm_temp_queue *enabled_vm_temp_queue = NULL;

// forward declarations
int lua_addban(lua_State *);
int lua_admin(lua_State *);
int lua_autodelim(lua_State *);
int lua_autokilloverbuffer(lua_State *);
int lua_chanlist(lua_State *);
int lua_chanuser(lua_State *);
int lua_chanusers(lua_State *);
int lua_crc32(lua_State *);
int lua_debug(lua_State *);
int lua_delban(lua_State *);
int lua_disconnect(lua_State *);
int lua_enablevm(lua_State *);
int lua_flashcheck(lua_State *);
int lua_include(lua_State *);
int lua_joinchannel(lua_State *);
int lua_killuser(lua_State *);
int lua_listban(lua_State *);
int lua_mainlooptrigger(lua_State *);
int lua_md5(lua_State *);
int lua_openserver(lua_State *);
int lua_partchannel(lua_State *);
int lua_rc4(lua_State *);
int lua_rc4hc(lua_State *);
int lua_rc4hd(lua_State *);
int lua_readline(lua_State *);
int lua_restart(lua_State *);
int lua_sendall(lua_State *);
int lua_sendallbutone(lua_State *);
int lua_sendchan(lua_State *);
int lua_sendchanbutone(lua_State *);
int lua_sendone(lua_State *);
int lua_setdelim(lua_State *);
int lua_setthrottle(lua_State *);
int lua_settimer(lua_State *);
int lua_shutdown(lua_State *);
int lua_userflag(lua_State *);
int lua_userinfo(lua_State *);
int check_func_params(lua_State *, const char *, int, int);
lua_State *create_lua_vm(int);
void free_enable_vm_temp_queue();
int load_lua_script(lua_State *, const char *);
int real_change_enabled_vm_server_flag(const char *, int, int);

// functions we must export inside each lua VM
//
typedef int (lua_FunctionProto) (lua_State *pL);
static const char *LUA_func_table_name[]={
    "openserver",	"debug",	"joinchannel",		"chanuser",	"partchannel",		"killuser",
    "chanlist",		"sendone",	"sendallbutone",	"sendall",	"sendchan",		"sendchanbutone",
    "admin",		"md5",		"rc4",			"settimer",	"setthrottle",		"include",
    "enablevm",		"setdelim",	"shutdown",		"restart",	"autodelim",		"addban",
    "delban",		"listban",	"userflag",		"userinfo",	"thread",		/* threads.c */
    "chanusers",	"flashcheck",	"rc4hc",		"rc4hd",	"mainlooptrigger",	"readline",
    "autokilloverbuffer",    "crc32",
    0
}; // LUA_func_table_name

static lua_FunctionProto *LUA_func_table_func[] ={
    lua_openserver,	lua_debug,	lua_joinchannel,	lua_chanuser,	lua_partchannel,	lua_killuser,
    lua_chanlist,	lua_sendone,	lua_sendallbutone,	lua_sendall,	lua_sendchan,		lua_sendchanbutone,
    lua_admin,		lua_md5,	lua_rc4,		lua_settimer,	lua_setthrottle,	lua_include,
    lua_enablevm,	lua_setdelim,	lua_shutdown,		lua_restart,	lua_autodelim,		lua_addban,
    lua_delban,		lua_listban,	lua_userflag,		lua_userinfo,	lua_thread,		/* threads.c */
    lua_chanusers,	lua_flashcheck,	lua_rc4hc,		lua_rc4hd,	lua_mainlooptrigger,	lua_readline,
    lua_autokilloverbuffer, lua_crc32,
    0
}; // LUA_func_table_func

static unsigned char LUA_func_table_levels[]={
    9,			1,		9,			9,		9,			9,
    9,			9,		9,			9,		9,			9,
    9,			1,		1,			9,		9,			9,
    9,			9,		9,			9,		9,			9,
    9,			9,		9,			9,		9,
    9,			9,		1,			1,		9,			9,
    1,			1,
    0
}; // LUA_func_table_levels


// LUA: addban(ip, reason)
// add a ban to bans list. Ban specified IP from allowing connecting daemon. Reason
// will be logged upon triggering the ban.
//
int lua_addban(lua_State *pL) {

    int numpars = check_func_params(pL, "addban", 2, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    const char *ip=lua_tostring(pL, 1);
    const char *reason=lua_tostring(pL, 2);
    
    int addbanres = addban(ip, reason);
    int savebanres = -9;
    if ( addbanres > 0 ) savebanres = save_bans_to_file();

    logme(LNORMAL, "Add ban for %s (%s) - Res: %d - %d", ip, reason, addbanres, savebanres);

    lua_pushnumber(pL, addbanres);
    return 1;
   
} // lua_addban


// LUA: admin(index[, 0/1])
// set a client as admin (owner server must be flagged as administrable, see openserver)
// returns current state is flag is missing
//
int lua_admin(lua_State *pL) {

    int numpars = check_func_params(pL, "admin", 1, 2);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int index = lua_tonumber(pL, 1);

    if ( numpars == 1 ) 	// get
	lua_pushnumber(pL, get_admin_client(index));
    else		 	// set
	lua_pushnumber(pL, set_admin_client(index, lua_tonumber(pL, 2)));

    lua_pushnumber(pL, 1);
    return 1;
    
} // int lua_admin


// LUA: autodelim(serv_index [,0/1])
// set or get autodelim flag for specified server
// if autodelim is 1, we'll automatically send packets delimitator after each send
//
int lua_autodelim(lua_State *pL) {

    int numpars = check_func_params(pL, "autodelim", 1, 2);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int res, index = lua_tonumber(pL, 1);
    
    if ( numpars == 1 ) 
	res = set_get_autodelim(index, -1); // get
    else
	res = set_get_autodelim(index, lua_tonumber(pL, 2)); // set    
    
    lua_pushnumber(pL, res);
    return 1;
        
} // lua_autodelim


// LUA: autokilloverbuffer(servindex [,0/1])
// set or get autokill flag for clients which sends packets too large (default 1)
//
int lua_autokilloverbuffer(lua_State *pL) {

    int numpars = check_func_params(pL, "autokilloverbuffer", 1, 2);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int res, index = lua_tonumber(pL, 1);
    
    if ( numpars == 1 ) 
	res = set_get_autokilloverbuffer(index, -1); // get
    else
	res = set_get_autokilloverbuffer(index, lua_tonumber(pL, 2)); // set    
    
    lua_pushnumber(pL, res);
    return 1;
        
} // lua_autokilloverbuffer


// LUA: flashcheck(servindex [,0/1])
// set or get flash cross domain check capabilities. if flashcheck = 1 the daemon will check
// if received text on the socket is a flash crossdomain.xml request and will trigger the policy file request event on the main script
//
int lua_flashcheck(lua_State *pL) {

    int numpars = check_func_params(pL, "flashcheck", 1, 2);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int res, index = lua_tonumber(pL, 1);
    
    if ( numpars == 1 ) 
	res = set_get_flashcheck(index, -1); // get
    else
	res = set_get_flashcheck(index, lua_tonumber(pL, 2)); // set    
    
    lua_pushnumber(pL, res);
    return 1;
        
} // lua_flashcheck


// LUA: chanlist(minusers)
// returns full opened channels list (filter by a minimum users inside channel)
// return: chan1, pass1, users1, chan2, pass2,users2 etc
//
int lua_chanlist(lua_State *pL) {

    int numpars = check_func_params(pL, "chanlist", 1, -1);

    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int minusers = lua_tonumber(pL, 1);

    // get full channels list
    struct chanlist *cl = get_fullchan_list(minusers);
    if (cl) {
	int nump = 0;
	while (cl) {
	    // name,pass,numusers
	    lua_pushstring(pL, cl->chan->name);
	    lua_pushstring(pL, cl->chan->pass);
	    lua_pushnumber(pL, cl->chan->numusers);
	    lua_pushnumber(pL, cl->chan->created_time);
	    nump+=4;
	    cl = cl->next;
	}
	free(cl);
	return nump;
    } else {
	// no channels found
	lua_pushnumber(pL, 0);
	return 1;
    }

} // int lua_chanlist


// LUA: chanuser(index)
// get opened channels list of specified user (same format of chanlist)
//
int lua_chanuser(lua_State *pL) {

    int numpars = check_func_params(pL, "chanuser", 1, -1);

    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
        
    int index = lua_tonumber(pL, 1);
    
    // get channels list of specified user
    struct chanlist *cl = get_chan_list(index);
    if (cl) {
	int nump = 0;
	while (cl) {
	    // name,pass,numusers,creation timestamp
	    lua_pushstring(pL, cl->chan->name);
	    lua_pushstring(pL, cl->chan->pass);
	    lua_pushnumber(pL, cl->chan->numusers);
	    lua_pushnumber(pL, cl->chan->created_time);
	    nump+=4;
	    cl = cl->next;
	}
	free(cl);
	return nump;
    } else {
	// nessun channel found
	lua_pushnumber(pL, 0);
	return 1;
    }
    
    
} // int lua_chanuser


// LUA: indexuser1,indexuser2.. = chanusers(channame)
// get users list of specified channel
//
int lua_chanusers(lua_State *pL) {

    int numpars = check_func_params(pL, "chanusers", 1, -1);

    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    // find channel and output users list
    const char *chan=lua_tostring(pL, 1);
    
    struct channel *ch = findchannel(chan);
    
    // channel not found
    if ( !ch ) {
	lua_pushnumber(pL, 0);
	return 1;
    }
    
    numpars = 0;
    struct clientlist *cl = ch->clientlist;
    
    while (cl) {
	lua_pushnumber(pL, cl->client->id);
	cl = cl->next;
	++numpars;
    }
    
    return numpars;
} // int lua_chanusers

// LUA: crc32(str [,len])
// calculate crc32 of specified buffer. If len is specified take 'len' bytes from str (usually used for binary strings)
//
int lua_crc32(lua_State *pL) {

    int numpars = check_func_params(pL, "crc32", 1, 2);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    const char *source = lua_tostring(pL, 1);
    unsigned int len;
    
    if ( numpars == 1 ) {
	len = strlen(source);
    } else {
	len = lua_tonumber(pL, 2);
    }
    
    // calculate crc32
    unsigned long _crc32 = crc32( source, len );    
    lua_pushnumber(pL, _crc32);
	    
    return 1;
} // crc32

// LUA: debug(text)
// log a line on logfile
//
int lua_debug(lua_State *pL) {

    int numpars = check_func_params(pL, "debug", 1, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    const char *text=lua_tostring(pL, 1);
    
    logme(LDEBUG, "LUA [DEBUG]: %s", text);
    
    return 0;
    
} // lua_debug


// LUA: delban(ip)
// remove ban from specified ip
//
int lua_delban(lua_State *pL) {

    int numpars = check_func_params(pL, "delban", 1, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    const char *ip=lua_tostring(pL, 1);
    
    int delbanres = delban(ip);
    int savebanres = -9;
    if ( delbanres > 0 ) savebanres = save_bans_to_file();

    logme(LNORMAL, "Del ban for %s - Res: %d - %d", ip, delbanres, savebanres);

    lua_pushnumber(pL, delbanres);
    return 1;

} // lua_delban


// enablevm("script.lua", servindex, 0/1)
// enable script on specified server (by index)
//
int lua_enablevm(lua_State *pL) {

    int numpars = check_func_params(pL, "enablevm", 3, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    int res;
    const char *script = lua_tostring(pL, 1);
    int servindex = lua_tonumber(pL, 2);
    int enable = lua_tonumber(pL, 3);
    
    // if config.lua was already loaded, we can enable vm changing directly servers structs
    // if it wasn't loaded, we have to queue changes on temporary struct, then we will sync it on
    // servers after load is complete
    if ( config_loaded == 0 ) {
    
	// queue changes
	logme(LVERYDEBUG, "Changing events handling (queued) - ServIndex: %d - Script: %s", servindex, script);
	res = 0;
	
	struct enabled_vm_temp_queue *enablevm = (struct enabled_vm_temp_queue *)malloc(sizeof(struct enabled_vm_temp_queue));
	enablevm->servindex = servindex;
	enablevm->enabled = (char)enable;
	enablevm->script = (char *)malloc(sizeof(char)*strlen(script)+1);
	enablevm->next = NULL;
	strncpy2(enablevm->script, script, strlen(script)+1);
	
	// place item as last item
	if ( !enabled_vm_temp_queue ) {
	    enabled_vm_temp_queue = enablevm;
	} else {
	    struct enabled_vm_temp_queue *tempitem = enabled_vm_temp_queue;
	    while ( tempitem->next )
		tempitem = tempitem->next;
	    tempitem->next = enablevm;	    
	}	
	
    } else {
    
	// real changes
	res = real_change_enabled_vm_server_flag(script, servindex, enable);	
    }
     
    lua_pushnumber(pL, res);
    return 1;
    
} // lua_enablevm


// LUA: include(script)
// include specified script
//
int lua_include(lua_State *pL) {

    int numpars = check_func_params(pL, "include", 1, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    const char *script = lua_tostring(pL, 1);
    
    // set include directory
    char fname[1024];
    snprintf(fname, 1024, "%s%s", CURRENT_SCRIPTS_PATH, script);
    
    // load script
    logme(LVERYDEBUG, "Trying script include: %s", fname);
    load_lua_script(pL, fname);
    
    lua_pushnumber(pL, 1);
    return 1;
	
} // lua_include


// LUA: joinchannel(client, name [,pass])
// join specified client to specified channel (using optional password)
//
int lua_joinchannel(lua_State *pL) {

    int numpars = check_func_params(pL, "joinchannel", 2, 3);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    int res;
    int index = lua_tonumber(pL, 1);
    const char *name = lua_tostring(pL, 2);

    // join
    if ( numpars == 2 )
	res = joinchannel(index, name, NULL);				// without password
    else
	res = joinchannel(index, name, lua_tostring(pL, 3));		// use password
    
    lua_pushnumber(pL, res);
    return 1;
        
} // int lua_joinchannel


// LUA: killuser(index)
// disconnect specified user
//
int lua_killuser(lua_State *pL) {

    int numpars = check_func_params(pL, "killuser", 1, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    int res;
    int index = lua_tonumber(pL, 1);
    
    res = killuser( index );
    
    lua_pushnumber(pL, res);
    return 1;
    
} // int lua_killuser


// LUA: listban( [pattern] )
// search in bans list using specified pattern
//
int lua_listban(lua_State *pL) {

    int numpars = check_func_params(pL, "listban", 0, 1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    // init bans search
    start_bans_navigate();
    
    // use a pattern or NULL to fetch all bans
    const char *pattern = ( numpars == 0 ? NULL : (const char *)lua_tostring(pL, 1) );
    
    // search in bans list
    struct bans_list *ban = get_next_ban( pattern );
    
    unsigned int numres = 0;
    
    // iterate bans list
    while (ban) {
	
	// result: ip, reason, numtriggers, tslast
	lua_pushstring(pL, ban->ip);
	lua_pushstring(pL, ban->reason);
	lua_pushnumber(pL, ban->numtriggers);    
	lua_pushnumber(pL, ban->tslast);    
	numres +=4;
	
	// search next ban
	ban = get_next_ban( pattern );
    } // while (ban)
    
    return numres;
    
} // lua_listban


// LUA: mainlooptrigger(steps)
// changes mainloop trigger steps interval for LUA onmainloop event to be called (if 0, default, the event won't be called)
//
int lua_mainlooptrigger(lua_State *pL) {

    int numpars = check_func_params(pL, "mainlooptrigger", 1, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    unsigned int steps = (unsigned int)lua_tonumber(pL, 1);
    unsigned int oldsteps = lua_onmainloop_trigger_max;
    lua_onmainloop_trigger_max = steps;	// set new steps value - daemon.c
    
    logme(LDEBUG, "LUA: Changing onmainloop steps interval (counter reset) - New: %d - Old: %d (each step: %d ms)", steps, oldsteps, MAINLOOP_SLEEP);
    lua_onmainloop_trigger_count = 0;	// reset numcalls counter
    
    lua_pushnumber(pL, oldsteps);
    return 1;
    
} // lua_mainlooptrigger


// LUA: md5(str [,len])
// calculate md5 of specified buffer. If len is specified take 'len' bytes from str (usually used for binary strings)
//
int lua_md5(lua_State *pL) {

    int numpars = check_func_params(pL, "md5", 1, 2);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    const char *source = lua_tostring(pL, 1);
    unsigned int len;
    
    if ( numpars == 1 ) {
	len = strlen(source);
    } else {
	len = lua_tonumber(pL, 2);
    }
    
    // calculate md5
    char dest[40];
    
    MD5(source, len, dest);
    
    lua_pushstring(pL, dest);
	    
    return 1;
} // md5

	    
// LUA: openserver(host,port,canadmin)
// open a listening server on host:port
// if canadmin = 1 we can have admin users on this server
//
int lua_openserver(lua_State *pL) {
    
    int numpars = check_func_params(pL, "openserver", 3, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    const char *host=lua_tostring(pL, 1);
    int port=lua_tonumber(pL, 2);
    int admin=lua_tonumber(pL, 3);
    		
    logme(LNORMAL, "LUA: - opening server on %s:%d - Admin: %s", host, port, (admin==1?"yes":"no"));
    
    int res = openserver(host, port, admin);
         
    lua_pushnumber(pL, res);    
    return 1;
} // lua_openserver


// LUA: partchannel(client, name)
// part specified client from specified channel
//
int lua_partchannel(lua_State *pL) {

    int numpars = check_func_params(pL, "partchannel", 2, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    int index = lua_tonumber(pL, 1);
    const char *name = lua_tostring(pL, 2);

    int res = partchannel(index, name);
    
    lua_pushnumber(pL, res);
    return 1;
    
} // int lua_partchannel

// LUA: rc4(str,pwd [,len])
// crypt specified str using specified pwd
// if len is also specified we'll take 'len' bytes from str (usually used for binary strings)
//
int lua_rc4(lua_State *pL) {

    int numpars = check_func_params(pL, "rc4", 2, 3);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    const char *source = lua_tostring(pL, 1);
    const char *pwd = lua_tostring(pL, 2);
    unsigned int len;
    
    if ( numpars == 2 ) {
	len = strlen(source);
    } else {
	len = lua_tonumber(pL, 3);
    }
    
    // rc4 crypt
    char *dest = malloc( sizeof(char) * len + 5 );
    
    if ( rc4( source, len, pwd, dest ) == len )
	lua_pushlstring(pL, dest, len);
    else
	lua_pushnumber(pL, -1);
	
    free(dest);
    
    return 1;
    
} // lua_rc4


// LUA: rc4hc(str,pwd [,len])
// crypt specified str using specified pwd
// if len is also specified we'll take 'len' bytes from str (usually used for binary strings)
// this is similar to rc4 function, but return value is Hex-encoded
//
int lua_rc4hc(lua_State *pL) {

    int numpars = check_func_params(pL, "rc4hc", 2, 3);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    const char *source = lua_tostring(pL, 1);
    const char *pwd = lua_tostring(pL, 2);
    unsigned int len;
    
    if ( numpars == 2 ) {
	len = strlen(source);
    } else {
	len = lua_tonumber(pL, 3);
    }
    
    // rc4 crypt with hex encrypt
    char *dest = malloc( sizeof(char) * len + 5 );
    char *dest2 = malloc( sizeof(char) * len * 2 + 5 );
    char tmpbuf[3];
    unsigned int i, c = 0;
        
    if ( rc4( source, len, pwd, dest ) == len ) {
	// ok, now hex-encode it
	for (i=0; i<len; i++) {
	    sprintf( tmpbuf, "%02x", (unsigned char)dest[i] );
	    dest2[c++] = tmpbuf[0];
	    dest2[c++] = tmpbuf[1];
	}
	// return encoded string
	lua_pushlstring(pL, dest2, c);
    } else // error
	lua_pushnumber(pL, -1);
	
    free(dest);
    free(dest2);
    
    return 1;
    
} // lua_rc4hc


// LUA: rc4hd(str,pwd)
// decrypt specified str using specified pwd
// this is similar to rc4 function, but this consider input value as hex-encoded (so: binary safe and no input len param should be required)
//
int lua_rc4hd(lua_State *pL) {

    int numpars = check_func_params(pL, "rc4hd", 2, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }


    const char *source = lua_tostring(pL, 1);
    const char *pwd = lua_tostring(pL, 2);
    
    // we need even characters for a good decrypt
    if ( strlen(source) % 2 != 0 ) {
	lua_pushnumber(pL, -2);
	return 1;
    }
        
    // rc4 decrypt, consider input as hex-encoded
    // get original rc4 string
    char *decsource = malloc( sizeof(char) * strlen(source) / 2 + 5 );
    char *dest = malloc( sizeof(char) * strlen(source) / 2 + 5 );
    char tmpbuf[3];
    unsigned int i, len;
    
    for (len = 0, i = 0; i < strlen(source); len++, i+=2 ) {
	sprintf( tmpbuf, "%c%c", source[i], source[i+1] );
	decsource[len] = strtol( tmpbuf, NULL, 16 );
    }

    // get original decrypted string
    if ( rc4( decsource, len, pwd, dest ) == len )
	lua_pushlstring(pL, dest, len);
    else
	lua_pushnumber(pL, -1);
    	
    free(decsource);
    free(dest);
    
    return 1;
    
} // lua_rc4hd


// LUA: res = readline(text)
// prompt local user for a line to be inserted through the shell.
// program execution is blocked until user pressed Enter
// libreadline must be installed and Daemon must be launched in single mode
//
int lua_readline(lua_State *pL) {

#ifdef HAVE_LIBREADLINE
    
    if ( cmdline.nodaemon != 1 ) {   // daemon mode
	logme(LWARNING, "Called readline but daemon is not launched in single mode!");
	lua_pushstring(pL, "");
	return 1;
    }
    
    int numpars = check_func_params(pL, "readline", 1, 0);
    
    if ( numpars < 0 ) {
	lua_pushstring(pL, "");
	return 1;
    }

    char *res;

    if (numpars == 0)
	res = readline ("");
    else
	res = readline ( (const char *)lua_tostring(pL, 1) );
    
    lua_pushstring(pL, res);
    free(res);
    return 1;
#else
    // if libreadline is not installed, returns an empty string and log to file
    logme(LWARNING, "Called readline but libreadline not installed!");
    lua_pushstring(pL, "");
    return 1;
#endif
    
} // lua_readline


// LUA: reboot()
// perform server reboot
//
int lua_restart(lua_State *pL) {
    rebootshutdown(2 /* reboot */);
    return 0;
} // lua_restart


// LUA: sendall(filter, line [,len])
// send buffer to all clients. 
// If len is also specified send 'len' bytes from line (used for binary strings)
// Packets delimitator is automatically added to line
//
int lua_sendall(lua_State *pL) {

    int numpars = check_func_params(pL, "sendall", 2, 3);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int res;
    long filter = lua_tonumber(pL, 1);
    const char *line = lua_tostring(pL, 2);

    if ( numpars == 2 )
	res = general_send(filter, NULL, -1, 0, line, strlen(line));
    else
	res = general_send(filter, NULL, -1, 0, line, lua_tonumber(pL, 3));
	
    lua_pushnumber(pL, res);
    return 1;
    
} // int lua_sendall


// LUA: sendchanbutone(name, exclude_index, filter, line [,len]) 
// send buffer to all users inside specified channel, but an user to exclude
// If len is also specified send 'len' bytes from line (used for binary strings)
// Packets delimitator is automatically added to line
//
int lua_sendchanbutone(lua_State *pL) {

    int numpars = check_func_params(pL, "sendchanbutone", 4, 5);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int res;
    const char *chan = lua_tostring(pL, 1);
    int index = lua_tonumber(pL, 2);
    long filter = lua_tonumber(pL, 3);
    const char *line = lua_tostring(pL, 4);

    if ( numpars == 4 )
	res = general_send(filter, chan, index, 1, line, strlen(line));
    else
	res = general_send(filter, chan, index, 1, line, lua_tonumber(pL, 5));
	
    lua_pushnumber(pL, res);
    return 1;
    
} // int lua_sendchanbutone


// LUA: sendchan(name, filter, line [,len])
// send buffer to all users inside specified channel
// If len is also specified send 'len' bytes from line (used for binary strings)
// Packets delimitator is automatically added to line
//
int lua_sendchan(lua_State *pL) {

    int numpars = check_func_params(pL, "sendchan", 3, 4);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int res;
    const char *chan = lua_tostring(pL, 1);
    long filter = lua_tonumber(pL, 2);
    const char *line = lua_tostring(pL, 3);

    if ( numpars == 3 )
	res = general_send(filter, chan, -1, 0, line, strlen(line));
    else
	res = general_send(filter, chan, -1, 0, line, lua_tonumber(pL, 4));
	
    lua_pushnumber(pL, res);
    return 1;
    
} // int lua_sendchan


// LUA: sendallbutone(exclude_index, filter, line [,len])
// send buffer to all users but one
// If len is also specified send 'len' bytes from line (used for binary strings)
// Packets delimitator is automatically added to line
//
int lua_sendallbutone(lua_State *pL) {

    int numpars = check_func_params(pL, "sendallbutone", 3, 4);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int res, index = lua_tonumber(pL, 1);
    long filter = lua_tonumber(pL, 2);
    const char *line = lua_tostring(pL, 3);

    if ( numpars == 3 )
	res = general_send(filter, NULL, index, 1, line, strlen(line));
    else
	res = general_send(filter, NULL, index, 1, line, lua_tonumber(pL, 4));
	
    lua_pushnumber(pL, res);
    return 1;
    
} // int lua_sendallbutone


// LUA: sendone(toindex, filter, line [,len])
// send buffer to specified client
// If len is also specified send 'len' bytes from line (used for binary strings)
// Packets delimitator is automatically added to line
//
int lua_sendone(lua_State *pL) {

    int numpars = check_func_params(pL, "sendone", 3, 4);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int res, index = lua_tonumber(pL, 1);
    long filter = lua_tonumber(pL, 2);
    const char *line = lua_tostring(pL, 3);

    if ( numpars == 3 )
	res = general_send(filter, NULL, index, 0, line, strlen(line));
    else
	res = general_send(filter, NULL, index, 0, line, lua_tonumber(pL, 4));
	
    lua_pushnumber(pL, res);
    return 1;
    
} // int lua_sendone


// LUA: setdelim(servindex, delim [, delimlen])
// set a new packets delimitator for specified server (default \r\n)
// we can specifify delimlen if we want to use binary delims (with \0 chars)
//
// NOTE: if delimlen is negative, we'll enable the delimitatorless mode for received packets
// (for sent packet we will add the specified delimitator (see autodelim), with length = abs(delimlen))
//
// In delimitatorless mode the daemon won't expect line delimitators, but instead each packet sent to daemon, 
// HAVE to be prefixed with packet length (unsigned short, big endian)
// For example (pseudo packet):
// setdelim(index,"\r\n", -2)
// autodelim(index,1)
// RECEIVE:
// <5><hello>
// SEND:
// sendone(index,"blabla",....)
// "blabla\r\n"
//
int lua_setdelim(lua_State *pL) {
    
    int numpars = check_func_params(pL, "setdelim", 2, 3);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int res, servindex = lua_tonumber(pL, 1);
    const char *newdelim = lua_tostring(pL, 2);
    int delimlen = ( numpars == 2 ? strlen(newdelim) : lua_tonumber(pL, 3) );
    
    res = set_new_delimitator(servindex, newdelim, delimlen);
    
    logme(LDEBUG, "%s - Delim. len: %d - Serv: %d - Result: %d", 
    	delimlen <= 0 ? "Enabling 'delimitatorless' mode (0 or less delimlen)" : "Changing server delimitator",
    	delimlen, servindex, res);
     
    lua_pushnumber(pL, res);
    return 1;
    
} // lua_setdelim


// LUA: setthrottle(maxclient, mininterval, rejectinterval)
// set general throttle configuration
// how many 'rejectinterval' seconds we must throttle user if we have 'maxclients' connections
// from same ip in 'mininterval' seconds
//
int lua_setthrottle(lua_State *pL) {

    int numpars = check_func_params(pL, "setthrottle", 3, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    throttle_allowed_num = (int)lua_tonumber(pL, 1);            
    throttle_allowed_interval = (int)lua_tonumber(pL, 2);
    throttle_reject_interval = (int)lua_tonumber(pL, 3);

    logme(LDEBUG, "Throttle config set: %d/%d/%d", throttle_allowed_num, throttle_allowed_interval, throttle_reject_interval);
    	    
    lua_pushnumber(pL, 1);
    return 1;
} // lua_setthrottle


// LUA: settimer(index, timeoutms)
// set timeout for timer event raising for specified client (in millisecs)
//
int lua_settimer(lua_State *pL) {

    int numpars = check_func_params(pL, "settimer", 2, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }

    int index = lua_tonumber(pL, 1);
    long timeout = (long)lua_tonumber(pL, 2);
    
    int res = settimer(index, timeout);

    logme(LDEBUG, "LUA: settimer - index: %d - timeout: %ld - res: %d", index, timeout, res);
    
    lua_pushnumber(pL, res);
    return 1;
        
} // lua_settimer


// LUA: shutdown()
// shutdown of the server
//
int lua_shutdown(lua_State *pL) {
    rebootshutdown(1 /* shutdown */);
    return 0;
} // lua_shutdown


// LUA: userflag(index [,flag])
// set or get user-defined client numeric flag
//
int lua_userflag(lua_State *pL) {

    int numpars = check_func_params(pL, "userflag", 1, 2);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int res, index = lua_tonumber(pL, 1);
    
    if ( numpars == 1 ) 
	res = set_get_userflag(0, index, -1 /*ignore*/); // get
    else
	res = set_get_userflag(1, index, lua_tonumber(pL, 2)); // set    
    
    lua_pushnumber(pL, res);
    return 1;
        
} // lua_userflag


// LUA: userinfo(index)
// get some useful userinfo
// ip, tsjoin, byterecv, bytesent, timeouttimer, numtimercalls
//
int lua_userinfo(lua_State *pL) {

    int numpars = check_func_params(pL, "userinfo", 1, -1);
    
    if ( numpars < 0 ) {
	lua_pushnumber(pL, numpars);
	return 1;
    }
    
    int index = lua_tonumber(pL, 1);
    
    if ( client_valid(index) == 1 ) {
	// client info. access directly CLIENTS struct (daemon.c)
	lua_pushstring(pL, CLIENTS[index]->ip);
	lua_pushnumber(pL, CLIENTS[index]->tsjoined);
	lua_pushnumber(pL, CLIENTS[index]->numrecv);
	lua_pushnumber(pL, CLIENTS[index]->numsend);
	lua_pushnumber(pL, CLIENTS[index]->timeout);
	lua_pushnumber(pL, CLIENTS[index]->calltimes);
	return 6;
    } else {
	// invalid client
	lua_pushnumber(pL, -1);
	return 1;
    }
        
} // lua_userinfo


// verify params count for specified function
// return -1 on error
// or numbers of passed parameters
//
int check_func_params(lua_State *pL, const char *function, int reqpars1, int reqpars2) {

    int numpars = lua_gettop(pL);

    if ( 
	( reqpars2 < 0 && numpars != reqpars1 ) ||
	( reqpars2 >= 0 && numpars != reqpars1 && numpars != reqpars2 )
    ) {
	logme(LERROR, "LUA: %s - called with wrong params # - used %d - needed %d,%d", function, numpars, reqpars1, reqpars2);
	return -1;
    }
    
    return numpars;
    
} // check_func_params


// create a new lua VM with all default modules loaded
// register all functions which should be exported
//
lua_State *create_lua_vm( int minfunclevel ) {
    lua_State *lua = luaL_newstate();
    luaL_openlibs(lua);
#if 0
    luaopen_base(lua);  /* opens the basic library */
    luaopen_math(lua);  /* opens the math library */
    luaopen_string(lua);  /* opens the string library */
    luaopen_table(lua);  /* opens the table library */
    //luaopen_package(LUA);  /* opens the package library */
    lua_pushcfunction(lua, luaopen_package);
    lua_pushliteral(lua, "package");
    lua_call(lua, 1, 0);
#endif

    // register all needed functions
    int loopcall=0;
    while ( LUA_func_table_name[loopcall] ) {
	// register function if it has required level
	if ( minfunclevel >= LUA_func_table_levels[loopcall] )
	    lua_register(lua, LUA_func_table_name[loopcall], LUA_func_table_func[loopcall]);
	//logme(LDEBUG, "Registering script function: %s", LUA_func_table_name[loopcall]);
	++loopcall;
    }
    
    return lua;
} // create_lua_vm


// destroy scripting engine and all allocated VM
//
int end_script_engine() {

    // destroy LUA
    // call unload event on all VM
    int s;
    for (s=0; s < NUMLUAVM; s++) {
	// call onunload
	lua_getglobal(LUA[s]->lua, "onunload");
        if( !lua_isfunction(LUA[s]->lua,-1) )  {
	    logme(LNOTICE, "unable to find onunload LUA function. VM: %d", s);
	    lua_pop(LUA[s]->lua,1);
	} else {
	    //lua_pushnumber(LUA, client->id);	
	    if (lua_pcall(LUA[s]->lua, 0 /* ingresso */, 0 /*1*/, 0) != 0) {
		logme(LERROR, "error while calling onunload LUA function. VM: %d - %s", s, lua_tostring (LUA[s]->lua, -1));
	    }
	}														
	// close LUA and free memory used by struct item
	lua_close(LUA[s]->lua);
	free(LUA[s]->filename);
	free(LUA[s]);
	logme(LDEBUG, "Closed LUA VM: %d", s);
    } // ciclo VM
    
    // free memory used by VM array
    free(LUA);
    NUMLUAVM = 0;
    
    // free memory used by temporary list for enabled_vm server's flag change queue
    free_enable_vm_temp_queue();
    
    return 0;
    
} // int end_script_engine

// load config script
// then load all scripts inside SCRIPT directory, opening a new VM for each one
//
int exec_init_script() {

    struct dirent *dir_entry_p;
    int s, flen;
    DIR *dir;
    int scriptfound = 0;
    char tmpfile[1024];
    
    // search config.lua (first step) and other scripts (following steps)
    for (s = 0; s <= 1; s++) {
	dir = opendir( CURRENT_SCRIPTS_PATH );
	if ( dir ) {
	    while( NULL != (dir_entry_p = readdir(dir))) {
	    
		flen = strlen(dir_entry_p->d_name);
	    
		if (flen < 5 || strcmp(dir_entry_p->d_name, "..") == 0 || strcmp(dir_entry_p->d_name, ".") == 0)
		    continue;

		// .lua	    
		if ( stricmp(&dir_entry_p->d_name[flen-4], ".lua") != 0 )
		    continue;
		
		snprintf(tmpfile, 1024, "%s%s", CURRENT_SCRIPTS_PATH, dir_entry_p->d_name);
		
		if ( s == 0 && stricmp("config.lua", dir_entry_p->d_name) == 0 ) {
		    // config found
		    lua_State *lua = create_lua_vm( 255 );	/* get all levels functions */
		    if ( load_lua_script(lua, tmpfile) == 0 )
			config_loaded = 1;
		    lua_close(lua);
		} else if ( s == 1 && stricmp("config.lua", dir_entry_p->d_name) != 0 ) {
		
		    // another script found
		    // reallocate array for lua VM and create a new one
		    lua_State *tmplua = create_lua_vm( 255 ); 	/* get all levels functions */
		    if ( load_lua_script(tmplua, tmpfile) == 0 ) {
			++scriptfound;
			
			// realloc array struct for LUAVM and alloc mem for filename
			LUA = (struct LUA **)realloc(LUA, sizeof(struct LUA *)*(NUMLUAVM+1));
			LUA[NUMLUAVM] = (struct LUA *)malloc(sizeof(struct LUA));
			
			// alloc and copy script filename inside struct
			LUA[NUMLUAVM]->filename = (char *)malloc(sizeof(char)*strlen(dir_entry_p->d_name)+1);
			strncpy2(LUA[NUMLUAVM]->filename, dir_entry_p->d_name, strlen(dir_entry_p->d_name)+1);
			
			// assign LUA vm pointer
			LUA[NUMLUAVM]->lua = tmplua;
			
			// call onload function
			lua_getglobal(LUA[NUMLUAVM]->lua, "onload");
    			if( !lua_isfunction(LUA[NUMLUAVM]->lua,-1) )  {
			    logme(LNOTICE, "unable to find onload LUA function. VM: %d", NUMLUAVM);
			    lua_pop(LUA[NUMLUAVM]->lua,1);
			} else {
			    if (lua_pcall(LUA[NUMLUAVM]->lua, 0 /* ingresso */, 0 /*1*/, 0) != 0) {
				logme(LERROR, "error while calling onload LUA function. VM: %d - %s", NUMLUAVM, lua_tostring (LUA[NUMLUAVM]->lua, -1));
			    }
			}
																	
			++NUMLUAVM;
			
		    } else {
			lua_close(tmplua);
		    }
		}
		
	    }
	    closedir(dir);
	}
    } // loop config/scripts
    
    if ( config_loaded == 0 ) {
	logme(LERROR, "Unable to find or load config file in: %s", CURRENT_SCRIPTS_PATH);
	return 0;
    }
    
    if (scriptfound == 0 ) {
	logme(LERROR, "Unable to find or load any script from: %s", CURRENT_SCRIPTS_PATH);
	return 0;
    }

    // ok
    return 1;
    
} // int exec_init_script()


// free memory used by temporary list which holds change queue for enabled_vm server's flag
//
void free_enable_vm_temp_queue() {

    struct enabled_vm_temp_queue *tmpqueue;
    
    while ( enabled_vm_temp_queue ) {
    
	tmpqueue = enabled_vm_temp_queue;		
	enabled_vm_temp_queue = enabled_vm_temp_queue->next;
	
	free(tmpqueue->script);				
	free(tmpqueue);
    }

} // free_enable_vm_temp_queue


// try loading specified file on specified lua VM
// 0: success
// <> 0: error
//
int load_lua_script(lua_State *lua, const char *file) {
    

    if ( luaL_loadfile(lua, file) == 0 ) {
	logme(LNOTICE, "Script file %s loaded", file);
	
	if( lua_pcall(lua, 0, 0, 0) == 0 ) {
	    logme(LNOTICE, "Script file %s executed", file);
	} else {
	    logme(LERROR, "Unable to execute script file: %s - %s", file, lua_tostring (lua, -1));
	    return -1;
	}
    } else {
	logme(LERROR, "Unable to load script file %s - %s", file, lua_tostring(lua, -1));
	return -2;
    }    

    // ok
    return 0;
} // int load_lua_script


// real change enabled_vm_events flag on specified server
// (this will change struct values, instead of config.load stage which will just queue changes...)
//
int real_change_enabled_vm_server_flag(const char *script, int servindex, int enable) {

    // search VM index, using its main script filename
    int i, vmuse = -1, res = -1;
    for (i = 0; i < NUMLUAVM; i++) {
        if ( stricmp(LUA[i]->filename, script) == 0 ) {
	    vmuse = i;
	    break;
	}
    }
    
    if ( vmuse == -1 ) {
	// vm not found
	logme(LERROR, "real_change_enabled_vm_server_flag: Cannot find specified VM: %s", script);
	res = -1;
    } else {
	// vm found
	logme(LDEBUG, "real_change_enabled_vm_server_flag: Changing events handling (real) - VM: %d - Script: %s - Serv: %d - Value: %d", vmuse, script, servindex, enable);
	res = set_enabled_vm_server(servindex, vmuse, enable);
    }

    return res;
} // int real_change_enabled_vm_server_flag


// signal client connection or disconnection to all opened lua VM, which are enabled on client's server
//
int signal_client_connection(char connected, struct sockclient *client) 
{
    int s;
    int clientid = client->id; // save client id (could be killed later)
    char ip_address[INET_ADDRSTRLEN];
    
    for (s=0; s < NUMLUAVM; s++) {
    	
	// continue to next VM if this one is not enabled to receive events from client's server
	if ( client->server->enabled_vm_events[ s ] == 0 )
	    continue;
	
	// call connect/disconnect function
	if ( connected == 1 )
	    lua_getglobal(LUA[s]->lua, "onconnect");
	else
	    lua_getglobal(LUA[s]->lua, "ondisconnected");
	
        if( !lua_isfunction(LUA[s]->lua,-1) )  {
	    logme(LNOTICE, "unable to find %s LUA function. VM: %d", (connected==1?"onconnect":"ondisconnected"), s);
	    lua_pop(LUA[s]->lua,1);
	} else {
	    
	    lua_pushnumber(LUA[s]->lua, client->id);
	    if ( connected == 1 )
	    {
	    	// second parameter, IP for onconnect
	    	inet_ntop(AF_INET, &(client->addr.sin_addr), ip_address, INET_ADDRSTRLEN);
		lua_pushstring(LUA[s]->lua, ip_address);
	    }
	    
	    if (lua_pcall(LUA[s]->lua, 
		( connected == 1 ? 2 : 1 ) /* # passed params */, 
		0 /*1*/, 0) != 0) {
		logme(LERROR, "error while calling %s LUA function. VM: %d - %s", (connected==1?"onconnect":"ondisconnected"), s, lua_tostring (LUA[s]->lua, -1));
	    }
	}														

	// if client is no more valid (could be killed after LUA event), interrupt scripts events for all VM
	if ( client_valid(clientid) == 0 )
	    break;
	    
    } // VM loop

    return 0;
} // int signal_client_connection


// signal all VM the onmainloop event
//
int signal_onmainloop_event()
{
    int s;
    
    for (s=0; s < NUMLUAVM; s++) {
    
	// call function
	lua_getglobal(LUA[s]->lua, "onmainloop");
	
        if( lua_isfunction(LUA[s]->lua,-1) )  {
        
	    lua_pushnumber(LUA[s]->lua, lua_onmainloop_trigger_count);
	    
	    if (lua_pcall(LUA[s]->lua, 1 /* # passed params */, 0 /*1*/, 0) != 0) {
		logme(LERROR, "error while calling %s LUA function. VM: %d - %s", "onmainloop", s, lua_tostring (LUA[s]->lua, -1));
	    }
        }
    
    } // for (s=0; s < NUMLUAVM; s++) {
    
    return 0;
    
} // int signal_onmainloop_event()


// signal all VM a registered timer event
//
int signal_timer_event(struct sockclient *client) 
{
    int s;
    int clientid = client->id; // save client id (could be killed later)
    
    for (s=0; s < NUMLUAVM; s++) {
    	
	// continue to next VM if this one is not enabled to receive events from client's server
	if ( client->server->enabled_vm_events[ s ] == 0 )
	    continue;
	    
	// call function
	lua_getglobal(LUA[s]->lua, "ontimer");
	
        if( !lua_isfunction(LUA[s]->lua,-1) )  {
	    logme(LNOTICE, "unable to find ontimer LUA function. VM: %d", s);
	    lua_pop(LUA[s]->lua,1);
	} else {
	    
	    lua_pushnumber(LUA[s]->lua, client->id);
	    lua_pushnumber(LUA[s]->lua, client->calltimes);
	    
	    if (lua_pcall(LUA[s]->lua, 2 /* # passed params */, 0 /*1*/, 0) != 0) {
		logme(LERROR, "error while calling ontimer LUA function. VM: %d - %s", s, lua_tostring (LUA[s]->lua, -1));
	    }
	}														

	// if client is no more valid (could be killed after LUA event), interrupt scripts events for all VM
	if ( client_valid(clientid) == 0 )
	    break;
	    
    } // VM loop
    
    return 0;
    
} // signal_timer_event


// signal a read/write event to all VM which are enabled on client's server
//
int signal_line_read_write(struct sockclient *client, const char *line, int len, char iswrite) {

    // log debug
    logme(LVERYDEBUG, "[%s] %s buffer - (sockID: %ld) - Len: %ld", client->ip, (iswrite==1?"Output":"Input"), client->id, len);
    
    int s;
    int clientid = client->id; // save client id (could be killed later)
    
    for (s=0; s < NUMLUAVM; s++) {
    	
	// continue to next VM if this one is not enabled to receive events from client's server
	if ( client->server->enabled_vm_events[ s ] == 0 )
	    continue;
	    
	// call function
	if (iswrite == 1)
	    lua_getglobal(LUA[s]->lua, "onwrite");
	else
	    lua_getglobal(LUA[s]->lua, "onread");
	
        if( !lua_isfunction(LUA[s]->lua,-1) )  {
	    logme(LNOTICE, "unable to find %s LUA function. VM: %d", (iswrite==1 ? "onwrite" : "onread"), s);
	    lua_pop(LUA[s]->lua,1);
	} else {
	    
	    lua_pushnumber(LUA[s]->lua, client->id);
	    lua_pushlstring(LUA[s]->lua, line, len);
	    
	    if (lua_pcall(LUA[s]->lua, 2 /* # passed params */, 0 /*1*/, 0) != 0) {
		logme(LERROR, "error while calling %s LUA function. VM: %d - %s", (iswrite==1 ? "onwrite" : "onread"), s, lua_tostring (LUA[s]->lua, -1));
	    }
	}														

	// if client is no more valid (could be killed after LUA event), interrupt scripts events for all VM
	if ( client_valid(clientid) == 0 )
	    break;
	    
    } // VM loop

    return 0;
} // int signal_line_read_write


// sync temporary change queue for enabled_vm server's flag and free memory used by list 
void sync_enabled_vm_temp_queue() {

    struct enabled_vm_temp_queue *tmpitem = enabled_vm_temp_queue;
    
    while (tmpitem) {
	real_change_enabled_vm_server_flag(tmpitem->script, tmpitem->servindex, (char)tmpitem->enabled);
	tmpitem = tmpitem->next;
    }
    
    free_enable_vm_temp_queue();

} // void sync_enabled_vm_temp_queue() {
