/*
 Copyright (c) 2011, Daniele Maiorana <tarokker@tiscali.it>
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.
    * Neither the name of the creator nor the names of its contributors may be used to endorse or promote products derived from this software 
without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef EXTERN_INCLUDE__H_
#define EXTERN_INCLUDE__H_


#include <lua.h>
#include <lauxlib.h>
#include <lualib.h> 
#include "struct.h"
#include <pthread.h>

// bans.c functions
int addban(const char *, const char *);
int delban(const char *);
void clear_bans();
int is_banned(char *ip);
int load_bans_from_file();
int save_bans_to_file();
void start_bans_navigate();
struct bans_list *get_next_ban(const char *);

// scripts.c functions
extern int NUMLUAVM;
extern lua_State *create_lua_vm(int);
extern int load_lua_script(lua_State *, const char *);
extern int end_script_engine();
extern int exec_init_script();
extern int signal_line_read_write(struct sockclient *,const char *, int, char);
extern int signal_client_connection(char, struct sockclient *);
extern int signal_onmainloop_event();
extern int signal_timer_event(struct sockclient *);
extern void sync_enabled_vm_temp_queue();

// util.c functions
extern FILE *logfp;
extern char CURRENT_SCRIPTS_PATH[];
extern unsigned long crc32( const void *buf, int bufLen );
extern int logme(int level, const char *, ...);
extern int match(const char *, const char *);
extern void output(const char *pattern, ...);
extern char parse_command(int, char **);
extern void print_help();
extern int rc4(const char *, int, const char *, char *);
int stricmp(const char *, const char *);
char *strncpy2(char *, const char *, size_t);

// md5.c functions
extern void MD5(const char *, unsigned int, char *);

// daemon.c functions
extern struct sockclient **CLIENTS;
extern unsigned int throttle_allowed_num, throttle_allowed_interval, throttle_reject_interval;
extern unsigned int lua_onmainloop_trigger_max;
extern unsigned long lua_onmainloop_trigger_count;
extern int pid;
extern int client_valid(int);
extern int general_send(long, const char *, int, char, const char *, int);
extern int get_admin_client(int);
extern struct chanlist *get_chan_list(int);
extern struct chanlist *get_fullchan_list(int);
extern struct channel *findchannel(const char *);
extern int init();
extern int killuser(int);
extern int joinchannel(int, const char *, const char *);
extern int partchannel(int, const char *);
extern int mainloop();
extern int openserver(const char *, int, int);
extern void rebootshutdown(int);
extern int set_admin_client(int, int);
extern int set_get_autodelim(int, int);
extern int set_get_autokilloverbuffer(int, int);
extern int set_get_flashcheck(int, int);
extern long set_get_userflag(char, int, long);
extern int set_enabled_vm_server(int, int, int);
extern int set_new_delimitator(int, const char *, int);
extern int settimer(int, long);
extern void stop(int);

// base64.c functions
extern int b64_sdecode(char *, const char *);
extern int b64_sencode(char *, const char *);
extern int b64_decode(char *, const char *);
extern int b64_encode(char *, const char *, int);

// threads.c functions
extern pthread_mutex_t mutex_logfile;
extern int flush_threaded_return_values_list();
extern int free_threads_vars();
extern int init_threads_vars();
extern int lua_thread(lua_State *);

#endif // EXTERN_INCLUDE__H_
