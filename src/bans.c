/*
 Copyright (c) 2011, Daniele Maiorana <tarokker@tiscali.it>
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.
    * Neither the name of the creator nor the names of its contributors may be used to endorse or promote products derived from this software 
without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/

#include "defines.h"
#include "extern.h"
#include "struct.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

// banned users list
struct bans_list *bans = NULL;

// pointer used to navigate inside bans list
struct bans_list *bansiterator = NULL;

// forward declarations
int addban(const char *, const char *);



// add a ban in the list, saving it again on filesystem
//
int addban(const char *ip, const char *reason) {
    
    if ( !ip || strlen(ip) == 0 || !reason )
	return -1;
	
    // check if ban was already in list
    struct bans_list *tmpban = bans;
    
    while(tmpban) {
	// ban found, update ban reason and then exit
	if (stricmp(tmpban->ip, ip) == 0) {
	    free(tmpban->reason);
	    tmpban->reason = (char *)malloc(strlen(reason)+1);
	    strcpy(tmpban->reason, reason);
	    logme(LDEBUG, "Ban reason for %s updated: %s", ip, reason);
	    return 2;
	}
	tmpban = tmpban->next;
    }
    
    
    // if we're here, we must add ban
    tmpban = (struct bans_list *)malloc(sizeof(struct bans_list));
    tmpban->reason = (char *)malloc(strlen(reason)+1);
    tmpban->ip = (char *)malloc(strlen(ip)+1);
    strcpy(tmpban->reason, reason);
    strcpy(tmpban->ip, ip);
    
    tmpban->tslast = 0;
    tmpban->numtriggers = 0;
    
    // update lists
    tmpban->next = bans;
    bans = tmpban;
    
    logme(LDEBUG, "Ban added for %s - reason: %s", ip, reason);
    
    return 1;
    
} // addban


// clear bans list
//
void clear_bans() {
    struct bans_list *tmpban;
    while (bans) {
	tmpban = bans;
	bans = bans->next;
	free(tmpban->ip);
	free(tmpban->reason);
	free(tmpban);
    }
    logme(LDEBUG, "Bans list cleared");
} // clear_bans


// remove ban of specified ip
//
int delban(const char *ip) {
    struct bans_list *currban = bans, *prevban = NULL;
    while (currban) {
	// ban found, remove it
	if ( stricmp(currban->ip, ip) == 0 ) {
	    
	    if ( prevban )
		// middle -> A->[B]->C
		prevban->next = currban->next;
	    else
		// head -> [A]->B->C
		bans = currban->next;

	    free(currban->ip);
	    free(currban->reason);
	    free(currban);
	    		
	    return 1;
	}
	prevban = currban;
	currban = currban->next;
    }
    // not found
    return 0;
} // delban


// get next ban in banlist (filtering ip by strmatch or taking all bans if '*')
// return NULL if we haven't any more bans
//
struct bans_list *get_next_ban(const char *strmatch) {
    struct bans_list *ban;
    while (bansiterator) {
	ban = bansiterator;
	bansiterator = bansiterator->next;
	if ( match((strmatch?strmatch:"*"), ban->ip) == 0 )
	    return ban;
    }
    return NULL;
} // get_next_ban


// check if specified ip is banned
//
int is_banned(char *ip) {
    struct bans_list *ban = bans;
    while (ban) {
	if (stricmp(ban->ip, ip) == 0) {
	    ban->tslast = time(NULL);
	    ++ban->numtriggers;
	    logme(LDEBUG, "Incoming connection rejected. %s is banned. Reason: %s", ip, ban->reason);
	    return 1;
	}
	ban = ban->next;
    }
    return 0;
} // is_banned


// load bans list from filesystem
//
int load_bans_from_file() {
    char line[4096], BANSPATH[255];
    char *ip, *reason;
    int numbans = 0;
    snprintf( BANSPATH, 255, "%s%s", CURRENT_SCRIPTS_PATH, "bans.dat");
    FILE *fp = fopen(BANSPATH, "r");
    if ( fp ) {
	while ( fgets(line, 4096, fp) != NULL ) {
	    // split line
	    ip = strtok(line, "\t");
	    if ( ip && strlen(ip) > 0 ) {
		reason = strtok(NULL, "\t");
		if ( reason ) reason[strlen(reason)-1]='\0';	// remove \n from end of line
		// addban
		addban(ip, (reason?reason:""));
		++numbans;
	    }
	}
	fclose(fp);
	logme(LDEBUG, "Bans list loaded: %s. %d bans processed.", BANSPATH, numbans);
	return numbans;
    } else {
	// error
	logme(LDEBUG, "Bans file not found: %s. 0 bans processed.", BANSPATH, numbans);
	return -1;
    }        
} // load_bans_from_file


// save bans list to filesysem
//
int save_bans_to_file() {
    char line[4096], BANSPATH[255];
    int numbans = 0;
    snprintf( BANSPATH, 255, "%s%s", CURRENT_SCRIPTS_PATH, "bans.dat");
    FILE *fp = fopen(BANSPATH, "w");
    if ( fp ) {
	// iterate bans list and save it
	struct bans_list *tmpban = bans;
	while (tmpban) {
	    sprintf(line, "%s\t%s\n", tmpban->ip, tmpban->reason);
	    fputs(line, fp);
	    tmpban = tmpban->next;
	    ++numbans;
	}
	fclose(fp);
	logme(LDEBUG, "Bans list saved: %s. %d bans processed.", BANSPATH, numbans);
	return numbans;
    } else {
	// error while saving
	logme(LERROR, "Unable to save bans file");
	return -1;
    }
} // save_bans_to_file


// set bans list iterator pointer to first element
//
void start_bans_navigate()
{
    bansiterator = bans;
} // start_bans_navigate
