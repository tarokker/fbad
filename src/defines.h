/*
 Copyright (c) 2011, Daniele Maiorana <tarokker@tiscali.it>
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.
    * Neither the name of the creator nor the names of its contributors may be used to endorse or promote products derived from this software 
without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DEFINES_INCLUDE__H
#define DEFINES_INCLUDE__H

#define h_addr h_addr_list[0] /* for backward compatibility */

// useful funcs
#ifndef MAKEWORD
    #define MAKEWORD(low, high) ((unsigned int)(((unsigned char)(low)) | (((unsigned int)((unsigned char)(high))) << 8)))
#endif

// usleep
// http://groups.google.it/group/gnu.g++.help/browse_thread/thread/a3235e07379ce36b/5bb7ad442910fc14?lnk=st&q=implicit+declaration+of+function+usleep#5bb7ad442910fc14
#define _XOPEN_SOURCE 500 

#define ROOT_LOGFILES "logs/"
#define DEFAULT_SCRIPTSPATH "script/"

// log levels
#define LERROR 0
#define LWARNING 1
#define LNORMAL 2
#define LNOTICE 3
#define LDEBUG 4
#define LVERYDEBUG 5	// VERY verbose log
#define DEFAULT_LOG_LEVEL LNOTICE

// logs rotate types
#define LROTNONE 0
#define LROTDAY 1
#define LROTMONTH 2
#define LROTYEAR 3

// flash policy request sent by flash applications and checked if flashcheck is enabled on listening servers
#define FLASH_POLICY_REQUEST "<policy-file-request/>"

// usleep wait time in mainloop
#define MAINLOOP_SLEEP 100

// socket default listen queue
#define DEFAULT_LISTEN_QUEUE 1

// how many clients realloc when we need more of them
#define REALLOC_CLIENTS_PAGE 50

// listen address max size
#define MAX_SERVER_ADDRESS_LEN 512

// in-buffer max size
#define MAX_RECV_BUFFER 8192

// max size of packets delimitator
#define SIZE_SOCKDELIM 10
#define DEFAULT_SOCKDELIM "\r\n"
#define DEFAULT_SOCKDELIM_SIZE 2

// channel name max size
#define MAX_CHANNEL_NAME 128

// channel password max size
#define MAX_CHAN_PASSWORD 30

// channels hashlist length
#define CHANS_HASHLIST 513

// max length of ip addresses saved in clients struct
#define MAX_IP_LEN 18

#endif // DEFINES_INCLUDE__H
