#!/bin/sh
#
# Increment build number

VERSION=0.1

if [ -f src/version.h ] ; then
	BUILD=`fgrep '#define VERSION_BUILD' src/version.h | sed 's/^#define VERSION_BUILD.*"\([0-9]*\)".*$/\1/'`
	BUILD=`expr $BUILD + 1 2>/dev/null`
else
	BUILD=1
fi
if [ ! "$BUILD" ] ; then
	BUILD=1
fi
cat >src/version.h <<EOF

#define VERSION_BUILD	"$BUILD"

#define VERSION_NUMBER "$VERSION"
#define VERSION_STRBUILD "build #" VERSION_BUILD ", compiled " __DATE__ " " __TIME__
#define VERSION_FULL "FBAd"
EOF

